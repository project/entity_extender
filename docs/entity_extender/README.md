# Create entity extender

This example shows how to extend the `user` Drupal entity.

```php
namespace Drupal\module\Plugin\EntityExtender;

use Drupal\entity_extender\Plugin\EntityExtender\EntityExtender;

/**
 * @EntityExtenderPlugin("user:user")
 */
class User extends EntityExtender {
  public function getFullName(): string {
    // `$this->entity` here has an instance of the `\Drupal\user\UserInterface`.
    return \sprintf(
      '%s %s',
      $this->get('field_first_name'),
      $this->get('field_last_name'),
    );
  }
}
```

The snippet below allows getting the extender's instance.

```php
use Drupal\module\Plugin\EntityExtender\User;

/* @var \Drupal\entity_extender\EntityExtenderManagerInterface $entity_extender_manager */
$entity_extender_manager = \Drupal::service('entity_extender.manager');
$account = $entity_extender_manager
  ->getStorage('user', 'user')
  ->load(1);

$extender = $entity_extender_manager->getExtender($account);
\assert($extender instanceof User);
\assert($extender->getEntity() === $account);

print $extender->getFullName();
```

Use a runtime (non-default) plugin implementation for instantiating the extender.

```php
use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_extender\EntityExtenderManagerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\module\Plugin\EntityExtender\User;

// Extend the plugin's class by a custom one. It shouldn't have the annotation
// since will be created with additional arguments.
class CustomUser extends User {
  public function __construct(
    array $plugin,
    EntityInterface $entity,
    EntityExtenderManagerInterface $entity_extender_manager,
    TranslationInterface $string_translation,
    LoggerChannelFactoryInterface $logger_factory,
    // NOTE: this is the extra argument that must be passed to `getExtender()`.
    protected string $customFullName,
  ) {
    parent::__construct(
      $plugin,
      $entity,
      $entity_extender_manager,
      $string_translation,
      $logger_factory,
    );
  }

  public function getFullName(): string {
    return $this->customFullName;
  }
}

/* @var \Drupal\entity_extender\EntityExtenderManagerInterface $entity_extender_manager */
$entity_extender_manager = \Drupal::service('entity_extender.manager');
$account = $entity_extender_manager
  ->getStorage('user', 'user')
  ->load(1);

$extender = $entity_extender_manager->getExtender($account, CustomUser::class, 'Name');
\assert($extender instanceof CustomUser);
\assert($extender->getEntity() === $account);
\assert($extender->getFullName() === 'Name');
```
