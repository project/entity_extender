# Create entity storage extender

The entity storage extender is much like the entity extender - its purpose is to provide a storage implementation tied to `entity_type:bundle`.

## Use case

We use the `node` entity type and the `product` content type to work with products. The products can have discounts, be active/inactive, and many other attributes.

Load active products:

```php
$node_storage = \Drupal::entityTypeManager()->getStorage('node');
$active_products = $node_storage->loadByProperties([
  'type' => 'product',
  'field_product_status' => 'active',
]);
```

Load products with >= 20% discount:

```php
$node_storage = \Drupal::entityTypeManager()->getStorage('node');
$product_ids = $node_storage
  ->getQuery()
  ->condition('type', 'product')
  ->condition('field_product_discount', 20, '>=')
  ->execute();

$discounted_products = empty($product_ids)
  ? []
  : $node_storage->loadMultiple($product_ids);
```

## Implementation

- The storage extender must be the child of the storage class. If the `entity_extender_node` is enabled the parent for `ProductStorage` will be `Drupal\entity_extender_node\Plugin\EntityTypeExtender\NodeHandlerStorage`.
- Override the `public static function createInstance` to inject dependencies to a custom storage.

```php
namespace Drupal\module\Plugin\EntityStorageExtender;

use Drupal\node\NodeStorage;

/**
 * @EntityStorageExtenderPlugin("node:product")
 */
class ProductStorage extends NodeStorage {
  public const BUNDLE = 'product';
  public const STATUS_ACTIVE = 'active';
  public const STATUS_DISABLED = 'disabled';

  public function loadWithStatus(string $status): array {
    \assert(\in_array($status, [static::STATUS_ACTIVE, static::STATUS_DISABLED], TRUE));
    return $this->loadByProperties([
      'type' => static::BUNDLE,
      'field_product_status' => $status,
    ]);
  }

  public function loadWithDiscountGreaterOrEqual(int $discount): array {
    \assert($discount >= 0);
    $ids = $this
      ->getQuery()
      ->condition('type', static::BUNDLE)
      ->condition('field_product_discount', $discount, '>=')
      ->execute();

    return empty($ids) ? [] : $this->loadMultiple($ids);
  }
}
```

## Usage

```php
/* @var \Drupal\entity_extender\EntityExtenderManagerInterface $entity_extender_manager */
$entity_extender_manager = \Drupal::service('entity_extender.manager');
/* @var \Drupal\module\Plugin\EntityStorageExtender\ProductStorage $product_storage */
$product_storage = $entity_extender_manager->getStorage('node', 'product');

$active_products = $product_storage->loadWithStatus($product_storage::STATUS_ACTIVE);
$discounted_products = $product_storage->loadWithDiscountGreaterOrEqual(20);
```
