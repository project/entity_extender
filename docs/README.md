# Entity Extender

This project provides only APIs and does nothing on its own. You will not see any effect by just installing the module however if you are a developer, it could be used with zero-configuration just out of the box.

First, let's get acquainted with the APIs the module introduce.

## Entity API

Drupal 8 entities are specifically typed objects that represent particular things. Simply speaking, each entity defined by a PHP class that implements some interfaces for defining an object.

Core-level entities types, such as `node`, `user`, or `taxonomy_term` de facto aren't replaceable, even despite it's technically possible. This makes these types pretty limited when it comes to complex business logic.

Consider the `field_first_name` and `field_last_name` for the `user` entity type. What you would do to get the full name? Usually something like this:

```php
$account = \Drupal::entityTypeManager()
  ->getStorage('user')
  ->load(1);

print sprintf('%s %s', $account->field_first_name->value, $account->field_last_name->value);
```

**What if you need this logic in several places?** Copy-paste doesn't seem a solid solution. **Is a unit test can be written for that code?** Seems barely possible. Now let's see how this can look having the wrapper around the standard entity.

```php
/* @var \Drupal\entity_extender\EntityExtenderManagerInterface $entity_extender_manager */
$entity_extender_manager = \Drupal::service('entity_extender.manager');

$account = $entity_extender_manager
  ->getStorage('user', 'user')
  ->load(1);

$extender = $entity_extender_manager->getExtender($account);

// You can pass the `$extender` down the hierarchy as it has the
// `getEntity()` method that returns an entity it's constructed for.
print $extender->getFullName();
```

Now we see that the `$extender` object has the `getFullName()` method. Of course, a PHP class having that method should be implemented at first, but we'll get back to this later. Now we generalized the `sprintf('%s %s', $account->field_first_name->value, $account->field_last_name->value)` and can reuse it across the app with ease, leveraging IDEs autocomplete. The extender's class can also get a unit test.

**Why wouldn't we just override the entity class in `hook_entity_type_alter()`?** While this is a solution for entity types with a single bundle (such as the `user`) then it will not work for, let's say, `node` that may have many bundles. The advantageous difference of the entity extender plugin is that it ties to the entity type and its bundle. Therefore you also can benefit by using constructions like `$entity instanceof NodeArticle` instead of `$entity->getEntityTypeId() === 'node' && $entity->bundle() === 'article'`.

This is just a simple scenario however it answers important questions of the complex apps and gives a developer the context for building a business logic around the entities.

## Entity type API

Drupal 8 entity types allow describing various aspects of entities, such as storage, access controllers, view builders, etc. These things, of course, are prebuilt with the core itself and there are mechanisms for extending the entity types, such as `hook_entity_type_alter()`.

The `entity_extender` project provides the enhanced plugin-based API to extend any entity type. All the plugins applied within the previously mentioned `hook_entity_type_alter()`, so literally the API could be considered as a wrapper for certain overrides made within that hook. The entity type extender can affect only the handlers that can be gotten via the `getHandlerClass()` and set using the `setHandlerClass()` of `Drupal\Core\Entity\EntityTypeInterface`.

A developer can define own handler types while the `entity_extender` ships with those for `access`, `storage`, and `view_builder` handlers.

## Built-in extended types

- Core
  - `node`
  - `user`
- Contrib
  - `group`

To enable one of them you'll need to install the appropriate module - `entity_extender_ENTITY_TYPE` (e.g. `drush en entity_extender_node`). The extended types enable extra capabilities for the entity extender plugins. Implement any of the following interfaces by the extender plugin to opt-in to the handling:

- `Drupal\entity_extender\Plugin\EntityExtender\EntityAccessInterface` - provide custom access checks that are fired before the core's ones.
- `Drupal\entity_extender\Plugin\EntityExtender\EntityFieldAccessInterface` - provide custom access checks for entity's fields that are fired before the core's ones.
- `Drupal\entity_extender\Plugin\EntityExtender\EntityLifecycleInterface` - enable `preSave()`, `postSave()` and other lifecycle methods for the entity extender plugins.
- `Drupal\entity_extender\Plugin\EntityExtender\EntityLifecycleTranslationInterface` - enable `onCreateTranslation()`, `onInsertTranslation()`, and `onPostDeleteTranslation()` lifecycle events for the entity extender plugin.
- `Drupal\entity_extender\Plugin\EntityExtender\EntityViewInterface` - alter renderable array for the entity view.

## Further reading

- [Create entity extender](./entity_extender)
- [Create entity storage extender](./entity_storage_extender)
- [Create entity type extender](./entity_type_extender)
