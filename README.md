# Entity Extender

Extend Drupal entities and entity types by the use of plugins.

<!--break-->

## Dependencies

- PHP 8.0+

## Links

- [Documentation](./docs)
