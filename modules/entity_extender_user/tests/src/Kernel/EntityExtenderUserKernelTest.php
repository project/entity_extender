<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_extender_user\Kernel;

use Drupal\entity_extender_user\Plugin\EntityTypeExtender\UserEntityTypeExtender;
use Drupal\Tests\entity_extender\Kernel\EntityExtenderKernelTest;

/**
 * Tests module API.
 *
 * @group entity_extender
 */
class EntityExtenderUserKernelTest extends EntityExtenderKernelTest {

  /**
   * {@inheritdoc}
   */
  protected const ENTITY_TYPE_EXTENDER = UserEntityTypeExtender::class;

  /**
   * {@inheritdoc}
   */
  protected const ENTITY_CREATE_DATA = [
    'name' => 'exampleUserName',
    'mail' => 'example@site.com',
  ];

}
