<?php

declare(strict_types=1);

namespace Drupal\entity_extender_user\Plugin\EntityTypeExtender;

use Drupal\entity_extender\Plugin\EntityTypeExtender\EntityHandlerTraitStorage;
use Drupal\user\UserStorage;

/**
 * The extended storage handler for users.
 */
class UserHandlerStorage extends UserStorage {

  use EntityHandlerTraitStorage;

}
