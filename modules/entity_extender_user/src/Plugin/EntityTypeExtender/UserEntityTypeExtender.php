<?php

declare(strict_types=1);

namespace Drupal\entity_extender_user\Plugin\EntityTypeExtender;

use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\entity_extender\Plugin\EntityTypeExtender\EntityHandlerDefinitionAccess;
use Drupal\entity_extender\Plugin\EntityTypeExtender\EntityHandlerDefinitionStorage;
use Drupal\entity_extender\Plugin\EntityTypeExtender\EntityHandlerDefinitionView;
use Drupal\entity_extender\Plugin\EntityTypeExtender\EntityTypeExtender;
use Drupal\user\UserAccessControlHandler;
use Drupal\user\UserStorage;

/**
 * Defines the extended entity type.
 *
 * @EntityTypeExtenderPlugin("user")
 */
class UserEntityTypeExtender extends EntityTypeExtender {

  /**
   * {@inheritdoc}
   */
  public function getHandlers(): array {
    return [
      new EntityHandlerDefinitionAccess(
        defaultClass: UserAccessControlHandler::class,
        replacingClass: UserHandlerAccess::class,
      ),
      new EntityHandlerDefinitionStorage(
        defaultClass: UserStorage::class,
        replacingClass: UserHandlerStorage::class,
      ),
      new EntityHandlerDefinitionView(
        defaultClass: EntityViewBuilder::class,
        replacingClass: UserHandlerView::class,
      ),
    ];
  }

}
