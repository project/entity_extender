<?php

declare(strict_types=1);

namespace Drupal\entity_extender_user\Plugin\EntityTypeExtender;

use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\entity_extender\Plugin\EntityTypeExtender\EntityHandlerTraitAccess;
use Drupal\user\UserAccessControlHandler;

/**
 * The extended access handler for users.
 */
class UserHandlerAccess extends UserAccessControlHandler implements EntityHandlerInterface {

  use EntityHandlerTraitAccess;

}
