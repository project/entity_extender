<?php

declare(strict_types=1);

namespace Drupal\entity_extender_user\Plugin\EntityTypeExtender;

use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\entity_extender\Plugin\EntityTypeExtender\EntityHandlerTraitView;

/**
 * The extended view builder for users.
 */
class UserHandlerView extends EntityViewBuilder {

  use EntityHandlerTraitView;

}
