<?php

declare(strict_types=1);

namespace Drupal\entity_extender_node\Plugin\EntityTypeExtender;

use Drupal\entity_extender\Plugin\EntityTypeExtender\EntityHandlerTraitView;
use Drupal\node\NodeViewBuilder;

/**
 * The extended view builder for nodes.
 */
class NodeHandlerView extends NodeViewBuilder {

  use EntityHandlerTraitView;

}
