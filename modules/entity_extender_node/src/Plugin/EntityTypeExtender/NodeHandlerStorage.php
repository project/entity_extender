<?php

declare(strict_types=1);

namespace Drupal\entity_extender_node\Plugin\EntityTypeExtender;

use Drupal\entity_extender\Plugin\EntityTypeExtender\EntityHandlerTraitStorage;
use Drupal\node\NodeStorage;

/**
 * The extended storage handler for nodes.
 */
class NodeHandlerStorage extends NodeStorage {

  use EntityHandlerTraitStorage;

}
