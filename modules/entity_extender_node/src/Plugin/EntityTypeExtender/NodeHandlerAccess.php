<?php

declare(strict_types=1);

namespace Drupal\entity_extender_node\Plugin\EntityTypeExtender;

use Drupal\entity_extender\Plugin\EntityTypeExtender\EntityHandlerTraitAccess;
use Drupal\node\NodeAccessControlHandler;

/**
 * The extended access handler for nodes.
 */
class NodeHandlerAccess extends NodeAccessControlHandler {

  use EntityHandlerTraitAccess;

}
