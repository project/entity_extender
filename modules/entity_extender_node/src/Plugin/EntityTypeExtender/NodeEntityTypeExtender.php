<?php

declare(strict_types=1);

namespace Drupal\entity_extender_node\Plugin\EntityTypeExtender;

use Drupal\entity_extender\Plugin\EntityTypeExtender\EntityHandlerDefinitionAccess;
use Drupal\entity_extender\Plugin\EntityTypeExtender\EntityHandlerDefinitionStorage;
use Drupal\entity_extender\Plugin\EntityTypeExtender\EntityHandlerDefinitionView;
use Drupal\entity_extender\Plugin\EntityTypeExtender\EntityTypeExtender;
use Drupal\node\NodeAccessControlHandler;
use Drupal\node\NodeStorage;
use Drupal\node\NodeViewBuilder;

/**
 * Defines the extended entity type.
 *
 * @EntityTypeExtenderPlugin("node")
 */
class NodeEntityTypeExtender extends EntityTypeExtender {

  /**
   * {@inheritdoc}
   */
  public function getHandlers(): array {
    return [
      new EntityHandlerDefinitionAccess(
        defaultClass: NodeAccessControlHandler::class,
        replacingClass: NodeHandlerAccess::class,
      ),
      new EntityHandlerDefinitionStorage(
        defaultClass: NodeStorage::class,
        replacingClass: NodeHandlerStorage::class,
      ),
      new EntityHandlerDefinitionView(
        defaultClass: NodeViewBuilder::class,
        replacingClass: NodeHandlerView::class,
      ),
    ];
  }

}
