<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_extender_node\Functional;

use Drupal\entity_extender_node_lifecycle_test\Plugin\EntityExtender\NodeLifecycleTestEntityExtender;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests node lifecycle.
 *
 * @group entity_extender_node
 */
class NodeExtenderLifecycleTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $profile = 'minimal';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_extender_node_lifecycle_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalCreateContentType([
      'type' => NodeLifecycleTestEntityExtender::ENTITY_BUNDLE,
      'name' => 'Node lifecycle test',
    ]);

    $this->drupalLogin($this->drupalCreateUser([
      'bypass node access',
    ]));

    $this->resetAll();
  }

  public function test(): void {
    $assert_session = $this->assertSession();

    $this->drupalGet('node/add/' . NodeLifecycleTestEntityExtender::ENTITY_BUNDLE);
    $assert_session->statusCodeEquals(200);

    $this->submitForm(NodeLifecycleTestEntityExtender::getTestValues(TRUE), 'Save');
    $assert_session->pageTextContains('has been created.');

    $this->drupalGet('node/1/edit');
    $assert_session->statusCodeEquals(200);

    $this->submitForm(NodeLifecycleTestEntityExtender::getTestValues(FALSE), 'Save');
    $assert_session->pageTextContains('has been updated.');
  }

}
