<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_extender_node\Kernel;

use Drupal\entity_extender_node\Plugin\EntityTypeExtender\NodeEntityTypeExtender;
use Drupal\Tests\entity_extender\Kernel\EntityExtenderKernelTest;

/**
 * Tests module API.
 *
 * @group entity_extender
 */
class EntityExtenderNodeKernelTest extends EntityExtenderKernelTest {

  /**
   * {@inheritdoc}
   */
  protected const ENTITY_TYPE_EXTENDER = NodeEntityTypeExtender::class;

  /**
   * {@inheritdoc}
   */
  protected const ENTITY_EXTENDER_BUNDLE = 'test_node_type';

  /**
   * {@inheritdoc}
   */
  protected const ENTITY_CREATE_DATA = [
    'type' => self::ENTITY_EXTENDER_BUNDLE,
    'title' => 'Example node',
  ];

  /**
   * {@inheritdoc}
   */
  protected const ENTITY_BUNDLE_CREATE_DATA = [
    'type' => self::ENTITY_EXTENDER_BUNDLE,
    'name' => 'Test node type',
  ];

}
