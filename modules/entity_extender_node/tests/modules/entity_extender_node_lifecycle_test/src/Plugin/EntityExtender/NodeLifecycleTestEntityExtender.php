<?php

declare(strict_types=1);

namespace Drupal\entity_extender_node_lifecycle_test\Plugin\EntityExtender;

use Drupal\entity_extender\Plugin\EntityExtender\EntityExtender;
use Drupal\entity_extender\Plugin\EntityExtender\EntityLifecycleInterface;

/**
 * Defines the entity extender.
 *
 * @EntityExtenderPlugin("node:node_lifecycle_test")
 *
 * @property-read \Drupal\node\NodeInterface $entity
 */
class NodeLifecycleTestEntityExtender extends EntityExtender implements EntityLifecycleInterface {

  public const ENTITY_TYPE = 'node';
  public const ENTITY_BUNDLE = 'node_lifecycle_test';

  /**
   * The state of whether the node is new.
   *
   * @var bool
   */
  protected bool $isNew = FALSE;

  /**
   * Returns the title.
   *
   * @param bool $new
   *   The state of whether the node is new.
   *
   * @return string
   *   The title.
   */
  public static function getTitle(bool $new): string {
    return 'Test node (' . ($new ? 'creation' : 'update') . ')';
  }

  /**
   * Returns the values for a test node.
   *
   * @param bool $new
   *   The state of whether the node is new.
   *
   * @return array
   *   The values for a test node.
   */
  public static function getTestValues(bool $new): array {
    $values = [];
    $values['title[0][value]'] = static::getTitle($new);

    return $values;
  }

  /**
   * Asserts the test node.
   *
   * @param static $node
   *   The test node.
   * @param bool|null $new
   *   The state of whether the node is new.
   */
  public static function assertTestValues(self $node, bool $new = NULL): void {
    static $is_new = FALSE;

    // Store the value if it was passed.
    if ($new !== NULL) {
      $is_new = $new;
    }

    $actual = $node->label();
    $expected = static::getTitle($is_new);

    \assert($expected === $actual, \sprintf('"%s" === "%s"', $expected, $actual));
  }

  /**
   * {@inheritdoc}
   */
  public function onCreate(): void {
  }

  /**
   * {@inheritdoc}
   */
  public function onCreateRevision(): void {
  }

  /**
   * {@inheritdoc}
   */
  public function onPreSave(): void {
    static::assertTestValues($this, $this->entity->isNew());
  }

  /**
   * {@inheritdoc}
   */
  public function onInsert(): void {
    static::assertTestValues($this);
  }

  /**
   * {@inheritdoc}
   */
  public function onUpdate(): void {
    static::assertTestValues($this);
  }

  /**
   * {@inheritdoc}
   */
  public function onPreDelete(): void {
  }

  /**
   * {@inheritdoc}
   */
  public function onPostDelete(): void {
  }

  /**
   * {@inheritdoc}
   */
  public function onPreDeleteRevision(): void {
  }

  /**
   * {@inheritdoc}
   */
  public function onPostDeleteRevision(): void {
  }

}
