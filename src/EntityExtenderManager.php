<?php

declare(strict_types=1);

namespace Drupal\entity_extender;

use Drupal\Component\Assertion\Inspector;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\entity_extender\Annotation\EntityExtenderPlugin;
use Drupal\entity_extender\Component\Memo;
use Drupal\entity_extender\Plugin\EntityExtender\EntityExtender;
use Drupal\entity_extender\Plugin\EntityExtender\EntityExtenderHandlerInterface;

/**
 * The entity extender manager.
 */
class EntityExtenderManager extends DefaultPluginManager implements EntityExtenderManagerInterface {

  use Memo;

  /**
   * Constructs the plugin manager.
   *
   * @param \Traversable $namespaces
   *   A list of namespaces to look the plugins at.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   An instance of the `cache.discovery` service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   An instance of the `module_handler` service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   An instance of the `entity_type.manager` service.
   * @param \Drupal\entity_extender\EntityTypeExtenderManagerInterface $entityTypeExtenderManager
   *   An instance of the `entity_extender.entity_type.manager` service.
   * @param \Drupal\entity_extender\EntityStorageExtenderManagerInterface $entityStorageExtenderManager
   *   An instance of the `entity_extender.entity_storage.manager` service.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected EntityTypeExtenderManagerInterface $entityTypeExtenderManager,
    protected EntityStorageExtenderManagerInterface $entityStorageExtenderManager,
  ) {
    parent::__construct(
      'Plugin/EntityExtender',
      $namespaces,
      $module_handler,
      EntityExtender::class,
      EntityExtenderPlugin::class,
    );

    $this->setCacheBackend($cache_backend, $this->subdir);
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeManager(): EntityTypeManagerInterface {
    return $this->entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getStorage(string $entity_type_id, string $bundle): EntityStorageInterface {
    $plugin_id = "$entity_type_id:$bundle";

    if (!$this->entityStorageExtenderManager->hasDefinition($plugin_id)) {
      return $this->entityTypeManager->getStorage($entity_type_id);
    }

    return static::useMemo(
      function () use ($entity_type_id, $plugin_id): EntityStorageInterface {
        $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);

        if (NULL === $storage_class_base = $entity_type->getHandlerClass('storage')) {
          /* @see \Drupal\Core\Entity\EntityTypeManager::getHandler() */
          throw new InvalidPluginDefinitionException($entity_type_id, \sprintf(
            'The "%s" entity type did not specify a storage handler.',
            $entity_type_id,
          ));
        }

        $storage_extender_definition = $this->entityStorageExtenderManager->getDefinition($plugin_id);
        $storage_extender = $this->entityTypeManager->createHandlerInstance($storage_extender_definition['class'], $entity_type);
        \assert($storage_extender instanceof EntityStorageInterface, $storage_extender_definition['id']);

        // There is no sense in a storage extender if its class is the same
        // as of the original one, so we check the extender is a child.
        if (!\is_subclass_of($storage_extender, $storage_class_base, FALSE)) {
          throw new InvalidPluginDefinitionException($storage_extender_definition['id'], \sprintf(
            'The "%s" must be a child of "%s".',
            $storage_extender::class,
            $storage_class_base,
          ));
        }

        return $storage_extender;
      },
      $entity_type_id,
      $bundle,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getExtender(EntityInterface $entity, string $class = NULL, mixed ...$arguments): ?EntityExtender {
    return $this->doGetExtender($entity, [], $class, ...$arguments);
  }

  /**
   * {@inheritdoc}
   */
  public function getExtenderIfMemberOf(string $spec, EntityInterface $entity, string $class = NULL, mixed ...$arguments): ?EntityExtender {
    return $this->doGetExtender($entity, [$spec], $class, ...$arguments);
  }

  /**
   * Returns the entity extender.
   *
   * @template T of EntityExtender
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to create an extender for.
   * @param string[] $specs
   *   The list of fully-qualified class/interface names the extender must
   *   implement/extend. If at least one is missing the method return NULL.
   * @param class-string<T>|null $class
   *   The fully-qualified class name to create an instance of. It must extend
   *   the {@see EntityExtender}. Otherwise, the exception will be thrown.
   * @param mixed ...$arguments
   *   The arbitrary set of arguments to pass to the extender's constructor.
   *
   * @return T|null
   *   The entity extender.
   *
   * @throws \InvalidArgumentException
   *   When the plugin class, either custom or default, is not a subclass
   *   of {@see EntityExtender}.
   *
   * @example
   * Please note that this method memoize the value so before passing
   * new `...$arguments` you must take care of resetting the cache.
   * @code
   * \Drupal\entity_extender\Component\Memo::undoMemo(\Drupal\Core\Entity\EntityInterface $entity);
   * @endcode
   */
  final protected function doGetExtender(EntityInterface $entity, array $specs, ?string $class, mixed ...$arguments): ?EntityExtender {
    \assert(Inspector::assertAll(static fn (string $spec): bool => \class_exists($spec) || \interface_exists($spec), $specs));

    try {
      $definition = $this->getDefinition(\sprintf('%s:%s', $entity->getEntityTypeId(), $entity->bundle()));
      /* @var class-string<\Drupal\entity_extender\Plugin\EntityExtender\EntityExtender> $class */
      $class ??= $definition['class'];

      if (!\is_subclass_of($class, $this->pluginInterface)) {
        throw new \InvalidArgumentException(\sprintf(
          'The "%s" for instantiating "%s" must be a child of "%s".',
          $class,
          $definition['id'],
          $this->pluginInterface,
        ));
      }

      foreach ($specs as $spec) {
        if (!\is_subclass_of($class, $spec)) {
          return NULL;
        }
      }

      \assert(
        $definition['class'] === $class || $this->isFlawless($class, $definition['entity_type']),
        \sprintf('The "%s" entity extender class is flawed (%s).', $definition['id'], $class),
      );

      return static::useMemo(
        fn (): EntityExtender => $class::create(\Drupal::getContainer(), $this, $entity, $definition, ...$arguments),
        $entity,
        $class,
        ...$specs,
      );
    }
    catch (PluginNotFoundException) {
      return NULL;
    }
  }

  /**
   * Returns the state of whether the extender's class is flawless.
   *
   * @param string $class
   *   The entity extender's class.
   * @param string $entity_type_id
   *   The entity type ID.
   *
   * @return bool
   *   The state of whether the extender's class is flawless.
   */
  final protected function isFlawless(string $class, string $entity_type_id): bool {
    if (\is_subclass_of($class, EntityExtenderHandlerInterface::class)) {
      // The list of handlers' interfaces that are implemented by
      // the extender's class.
      $implemented = [];
      // The list of handlers' interfaces that can be implemented by
      // the extender's class.
      $supported = [];

      foreach (\class_implements($class) as $interface) {
        if (\is_subclass_of($interface, EntityExtenderHandlerInterface::class)) {
          $implemented[] = $interface;
        }
      }

      try {
        foreach ($this->entityTypeExtenderManager->createInstance($entity_type_id)->getHandlers() as $handler) {
          $supported[] = $handler->getEntityExtenderHandlersInterfaces();
        }

        $supported = \array_merge(...$supported);
      }
      catch (PluginException) {
      }

      // The list of interfaces, handlers for which are missing.
      $missing = \array_diff($implemented, $supported);

      if (!empty($missing)) {
        foreach ($missing as $interface) {
          \trigger_error(
            \sprintf('The "%s" is implemented by "%s" however there are no entity type extenders to provide functionality for it.', $interface, $class),
            \E_USER_WARNING,
          );
        }

        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  final protected function doGetDefinition(array $definitions, $plugin_id, $exception_on_invalid): ?array {
    try {
      // Try standard flow and throw an exception if a plugin cannot be found.
      return parent::doGetDefinition($definitions, $plugin_id, TRUE);
    }
    catch (PluginNotFoundException $e) {
      // Try to lookup a plugin by class.
      foreach ($definitions as $definition) {
        // The class is persistent since we disallow altering.
        /* @see alterDefinitions() */
        if ($definition['class'] === $plugin_id) {
          return $definition;
        }
      }

      if ($exception_on_invalid) {
        throw $e;
      }
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  final public function processDefinition(&$definition, $plugin_id): void {
    parent::processDefinition($definition, $plugin_id);
    \assert(
      $this->isFlawless($definition['class'], $definition['entity_type']),
      \sprintf('The "%s" entity extender class is flawed (%s).', $plugin_id, $definition['class']),
    );
  }

  /**
   * {@inheritdoc}
   */
  final public function createInstance($plugin_id, array $configuration = []): void {
    throw new \BadMethodCallException("This API is not supported - use `\Drupal::service('entity_extender.manager')->getExtender()`.");
  }

  /**
   * {@inheritdoc}
   */
  final public function getInstance(array $options): void {
    throw new \BadMethodCallException("This API is not supported - use `\Drupal::service('entity_extender.manager')->getExtender()`.");
  }

}
