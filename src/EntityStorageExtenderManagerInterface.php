<?php

declare(strict_types=1);

namespace Drupal\entity_extender;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Describes the entity storage extender manager.
 */
interface EntityStorageExtenderManagerInterface extends PluginManagerInterface {
}
