<?php

declare(strict_types=1);

namespace Drupal\entity_extender\Access;

use Drupal\Core\Access\AccessResultAllowed;

/**
 * Allows skipping Drupal core's access checks.
 *
 * @see \Drupal\entity_extender\Plugin\EntityTypeExtender\EntityHandlerTraitAccess::access()
 */
class AccessResultAllowedWithoutCoreAccessChecks extends AccessResultAllowed {}
