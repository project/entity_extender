<?php

declare(strict_types=1);

namespace Drupal\entity_extender;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\entity_extender\Annotation\EntityTypeExtenderPlugin;
use Drupal\entity_extender\Plugin\EntityTypeExtender\EntityTypeExtender;

/**
 * The extender's entity type manager.
 */
class EntityTypeExtenderManager extends DefaultPluginManager implements EntityTypeExtenderManagerInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler,
  ) {
    parent::__construct(
      'Plugin/EntityTypeExtender',
      $namespaces,
      $module_handler,
      EntityTypeExtender::class,
      EntityTypeExtenderPlugin::class,
    );

    $this->setCacheBackend($cache_backend, $this->subdir);
  }

  /**
   * {@inheritdoc}
   */
  public function extend(array $entity_types): void {
    foreach ($this->getDefinitions() as $definition) {
      if (isset($entity_types[$definition['id']])) {
        $this
          ->createInstance($definition['id'])
          ->apply($entity_types[$definition['id']]);
      }
    }
  }

}
