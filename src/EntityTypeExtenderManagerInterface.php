<?php

declare(strict_types=1);

namespace Drupal\entity_extender;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Describes the extender's entity type manager.
 *
 * @method \Drupal\entity_extender\Plugin\EntityTypeExtender\EntityTypeExtender createInstance(string $plugin_id, array $configuration = [])
 */
interface EntityTypeExtenderManagerInterface extends PluginManagerInterface {

  /**
   * Extends entity types.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface[] $entity_types
   *   The associative array of entity type definitions.
   *
   * @see \entity_extender_entity_type_alter()
   */
  public function extend(array $entity_types): void;

}
