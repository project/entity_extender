<?php

declare(strict_types=1);

namespace Drupal\entity_extender\Plugin\EntityTypeExtender;

use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\entity_extender\EntityTypeExtenderManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The base entity type extender.
 */
abstract class EntityTypeExtender extends PluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $definition) {
    \assert(!empty($definition['id']));
    parent::__construct([], $definition['id'], $definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $id, $definition): static {
    return new static($definition);
  }

  /**
   * Returns the list of handler to override.
   *
   * @return \Drupal\entity_extender\Plugin\EntityTypeExtender\EntityHandlerDefinition[]
   *   The list of handlers to override.
   */
  abstract public function getHandlers(): array;

  /**
   * Applies the modifications for a given entity type.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The default entity type definition to modify.
   *
   * @see \Drupal\entity_extender\EntityTypeExtenderManagerInterface::extend()
   */
  final public function apply(EntityTypeInterface $entity_type): void {
    $handlers = $this->getHandlers();
    $types = [];

    if (empty($handlers)) {
      \trigger_error(\sprintf('The "%s" does not provide any handlers so it existence is useless. Consider removing.', static::class), \E_USER_WARNING);
    }

    foreach ($handlers as $handler) {
      \assert($handler instanceof EntityHandlerDefinition);
      $type = $handler->getType();
      $id = \sprintf('%s %s', $this->pluginId, $type);
      \assert(!isset($types[$type]), $id);
      $required_trait = $handler->getRequiredTrait();
      // Make sure we override the default implementation.
      \assert($entity_type->getHandlerClass($type) === $handler->defaultClass, $id);
      // Make sure we extend the default implementation.
      \assert(\is_subclass_of($handler->replacingClass, $handler->defaultClass), $id);
      // Make sure the custom class implements the required interface.
      \assert(\is_subclass_of($handler->replacingClass, EntityHandlerInterface::class), $id);
      // Make sure the required trait uses the base one.
      \assert(\in_array(EntityHandlerTrait::class, \class_uses($required_trait), TRUE), $id);
      // Make sure the required trait is used by a custom implementation.
      \assert(\in_array($required_trait, \class_uses($handler->replacingClass), TRUE), $id);
      $entity_type->setHandlerClass($type, $handler->replacingClass);
      $types[$type] = TRUE;
    }
  }

}
