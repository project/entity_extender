<?php

declare(strict_types=1);

namespace Drupal\entity_extender\Plugin\EntityTypeExtender;

use JetBrains\PhpStorm\Immutable;

/**
 * The base entity handler definition.
 */
abstract class EntityHandlerDefinition {

  /**
   * Constructs the handler definition.
   *
   * @param string $defaultClass
   *   The handler class the entity type has by default.
   * @param string $replacingClass
   *   The handler class to set to the entity type.
   *   NOTE: this class must extend the default one.
   */
  public function __construct(
    #[Immutable]
    public string $defaultClass,
    #[Immutable]
    public string $replacingClass,
  ) {
  }

  /**
   * Returns the type ID of the entity handler.
   *
   * @return string
   *   The entity handler ID.
   */
  abstract public function getType(): string;

  /**
   * Returns the trait the handler class must user.
   *
   * @return string
   *   The trait the handler class must user.
   *
   * @see \Drupal\entity_extender\Plugin\EntityTypeExtender\EntityHandlerTrait
   */
  abstract public function getRequiredTrait(): string;

  /**
   * Returns the interfaces the extender could implement.
   *
   * @template T of \Drupal\entity_extender\Plugin\EntityExtender\EntityExtenderHandlerInterface
   *
   * @return class-string<T>[]
   *   The interfaces to implement by the entity extender plugin
   *   to opt into a specific entity handling.
   *
   * @see \Drupal\entity_extender\Plugin\EntityExtender\EntityExtenderHandlerInterface
   */
  abstract public function getEntityExtenderHandlersInterfaces(): array;

}
