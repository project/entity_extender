<?php

declare(strict_types=1);

namespace Drupal\entity_extender\Plugin\EntityTypeExtender;

use Drupal\Core\Entity\EntityHandlerBase;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\entity_extender\Component\Memo;
use Drupal\entity_extender\EntityExtenderManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Creates entity handler with the injected extenders manager.
 */
trait EntityHandlerTrait {

  use Memo;

  /**
   * An instance of the `entity_extender.manager` service.
   *
   * @var \Drupal\entity_extender\EntityExtenderManagerInterface
   */
  protected EntityExtenderManagerInterface $entityExtenderManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(mixed ...$arguments) {
    // The class that uses this trait must implement `EntityHandlerInterface`
    // in order to guarantee the `createInstance()` will be called.
    \assert(
      \is_subclass_of(static::class, EntityHandlerInterface::class),
      \sprintf('"%s" must implement "%s"', static::class, EntityHandlerInterface::class),
    );

    parent::__construct(...$arguments);
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    /* @see \Drupal\Core\Entity\EntityTypeManagerInterface::createHandlerInstance() */
    $instance = \is_subclass_of(parent::class, EntityHandlerInterface::class)
      ? parent::createInstance($container, $entity_type)
      : new static($entity_type);

    \assert($instance instanceof EntityHandlerBase);
    /* @noinspection PhpUndefinedFieldInspection */
    $instance->entityExtenderManager = $container->get('entity_extender.manager');

    return $instance;
  }

}
