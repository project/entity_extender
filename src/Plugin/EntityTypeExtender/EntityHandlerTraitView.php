<?php

declare(strict_types=1);

namespace Drupal\entity_extender\Plugin\EntityTypeExtender;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_extender\Plugin\EntityExtender\EntityViewInterface;

/**
 * The extended view builder for nodes.
 */
trait EntityHandlerTraitView {

  use EntityHandlerTrait;

  /**
   * {@inheritdoc}
   */
  protected function alterBuild(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode): void {
    parent::alterBuild($build, $entity, $display, $view_mode);

    if ($extender = $this->entityExtenderManager->getExtenderIfMemberOf(EntityViewInterface::class, $entity)) {
      \assert($extender instanceof EntityViewInterface);
      $extender->alterRenderableData($build, $display, $view_mode);
    }
  }

}
