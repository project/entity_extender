<?php

declare(strict_types=1);

namespace Drupal\entity_extender\Plugin\EntityTypeExtender;

use Drupal\entity_extender\Plugin\EntityExtender\EntityAccessInterface;
use Drupal\entity_extender\Plugin\EntityExtender\EntityFieldAccessInterface;

/**
 * Defines the entity handler type.
 *
 * @see \Drupal\Core\Entity\EntityTypeInterface::getAccessControlClass()
 */
class EntityHandlerDefinitionAccess extends EntityHandlerDefinition {

  /**
   * {@inheritdoc}
   */
  public function getType(): string {
    return 'access';
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredTrait(): string {
    return EntityHandlerTraitAccess::class;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityExtenderHandlersInterfaces(): array {
    return [
      EntityAccessInterface::class,
      EntityFieldAccessInterface::class,
    ];
  }

}
