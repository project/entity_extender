<?php

declare(strict_types=1);

namespace Drupal\entity_extender\Plugin\EntityTypeExtender;

use Drupal\entity_extender\Plugin\EntityExtender\EntityViewInterface;

/**
 * Defines the entity handler type.
 *
 * @see \Drupal\Core\Entity\EntityTypeInterface::getViewBuilderClass()
 */
class EntityHandlerDefinitionView extends EntityHandlerDefinition {

  /**
   * {@inheritdoc}
   */
  public function getType(): string {
    return 'view_builder';
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredTrait(): string {
    return EntityHandlerTraitView::class;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityExtenderHandlersInterfaces(): array {
    return [
      EntityViewInterface::class,
    ];
  }

}
