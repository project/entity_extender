<?php

declare(strict_types=1);

namespace Drupal\entity_extender\Plugin\EntityTypeExtender;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\RevisionableInterface;

/**
 * The extended storage handler for entities.
 */
trait EntityHandlerTraitStorage {

  use EntityHandlerTrait;

  /**
   * {@inheritdoc}
   */
  public function createRevision(RevisionableInterface $entity, $default = TRUE, $keep_untranslatable_fields = NULL) {
    $revision = parent::createRevision($entity, $default, $keep_untranslatable_fields);
    $this->lifecycle('revision_create', $revision);

    return $revision;
  }

  /**
   * {@inheritdoc}
   */
  protected function invokeFieldMethod($method, ContentEntityInterface $entity, ...$args): array {
    // Do not call `$this->lifecycle($method, $entity)` here as `$method`
    // may intersect with the other existing hooks, e.g. `delete`.
    if ($method === 'deleteRevision') {
      /* @see \Drupal\Core\Entity\ContentEntityStorageBase::deleteRevision() */
      $this->lifecycle('revision_predelete', $entity);
    }

    return parent::invokeFieldMethod($method, $entity, ...$args);
  }

  /**
   * {@inheritdoc}
   */
  protected function invokeHook($hook, EntityInterface $entity): void {
    parent::invokeHook($hook, $entity);
    $this->lifecycle($hook, $entity);
  }

  /**
   * Runs the lifecycle handler.
   */
  private function lifecycle(string $hook, EntityInterface $entity): void {
    foreach (EntityHandlerStorageLifecycles::LIFECYCLES as $interface => $methods) {
      if (isset($methods[$hook])) {
        // We must ensure the extender is created from scratch during each
        // lifecycle event. This is especially crucial during entity creation/edit
        // from UI, so the `presave`/`update`/`insert` has updated values.
        static::undoMemo($entity);

        if ($extender = $this->entityExtenderManager->getExtenderIfMemberOf($interface, $entity)) {
          \assert($extender instanceof $interface, $interface);
          $extender->{$methods[$hook]}();
        }

        // Only one hook can be called per method invocation, so stop here.
        break;
      }
    }
  }

}
