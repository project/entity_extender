<?php

declare(strict_types=1);

namespace Drupal\entity_extender\Plugin\EntityTypeExtender;

use Drupal\entity_extender\Plugin\EntityExtender\EntityLifecycleInterface;
use Drupal\entity_extender\Plugin\EntityExtender\EntityLifecycleTranslationInterface;

/**
 * Defines the entity handler type.
 *
 * @see \Drupal\Core\Entity\EntityTypeInterface::getStorageClass()
 */
class EntityHandlerDefinitionStorage extends EntityHandlerDefinition {

  /**
   * {@inheritdoc}
   */
  public function getType(): string {
    return 'storage';
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredTrait(): string {
    return EntityHandlerTraitStorage::class;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityExtenderHandlersInterfaces(): array {
    return [
      EntityLifecycleInterface::class,
      EntityLifecycleTranslationInterface::class,
    ];
  }

}
