<?php

declare(strict_types=1);

namespace Drupal\entity_extender\Plugin\EntityTypeExtender;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity_extender\Access\AccessResultAllowedWithoutCoreAccessChecks;
use Drupal\entity_extender\Plugin\EntityExtender\EntityAccessInterface;
use Drupal\entity_extender\Plugin\EntityExtender\EntityFieldAccessInterface;

/**
 * The extended access handler for entities.
 */
trait EntityHandlerTraitAccess {

  use EntityHandlerTrait;

  /**
   * Returns the access check result.
   *
   * @param class-string $interface
   *   The interface the entity extender class must implement.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to construct the extender for.
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   The account to check the access for.
   * @param bool $return_as_object
   *   The state of whether to return access check object or boolean.
   * @param \Closure $checker
   *   The access checker that takes the `$extender` and `$account`
   *   parameters and must return the {@see AccessResultInterface}.
   *
   * @return \Drupal\Core\Access\AccessResultInterface|bool|null
   *   The access check result or `null` if there is no extender for
   *   an `$entity` or its extender class doesn't implement `$interface`.
   */
  private function checkExtenderAccess(
    string $interface,
    EntityInterface $entity,
    ?AccountInterface $account,
    bool $return_as_object,
    \Closure $checker,
  ): AccessResultInterface|bool|null {
    if ($extender = $this->entityExtenderManager->getExtenderIfMemberOf($interface, $entity)) {
      $access = $checker($extender, $this->prepareUser($account));
      \assert($access instanceof AccessResultInterface);

      // The `allowed` and `neutral` states let us proceed to core's access
      // checks. Only forbiddance terminates access checks immediately.
      if ($access->isForbidden()) {
        return $return_as_object ? $access : FALSE;
      }

      if ($access instanceof AccessResultAllowedWithoutCoreAccessChecks) {
        return $return_as_object ? $access : TRUE;
      }
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function access(EntityInterface $entity, $operation, AccountInterface $account = NULL, $return_as_object = FALSE) {
    $access = $this->checkExtenderAccess(
      EntityAccessInterface::class,
      $entity,
      $account,
      $return_as_object,
      static fn (EntityAccessInterface $extender, AccountInterface $user): AccessResultInterface => static::useMemo(
        static fn (): AccessResultInterface => $extender->access($operation, $user),
        $operation,
        $user,
      ),
    );

    return $access ?? parent::access($entity, $operation, $account, $return_as_object);
  }

  /**
   * {@inheritdoc}
   */
  public function fieldAccess(
    $operation,
    FieldDefinitionInterface $field_definition,
    AccountInterface $account = NULL,
    FieldItemListInterface $items = NULL,
    $return_as_object = FALSE,
  ) {
    $access = $items === NULL ? NULL : $this->checkExtenderAccess(
      EntityFieldAccessInterface::class,
      $items->getEntity(),
      $account,
      $return_as_object,
      static fn (EntityFieldAccessInterface $extender, AccountInterface $user): AccessResultInterface => static::useMemo(
        static fn (): AccessResultInterface => $extender->fieldAccess($operation, $user, $items, $field_definition),
        $field_definition->getName(),
        $items->getString(),
        $operation,
        $user,
      ),
    );

    return $access ?? parent::fieldAccess($operation, $field_definition, $account, $items, $return_as_object);
  }

}
