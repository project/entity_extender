<?php

declare(strict_types=1);

namespace Drupal\entity_extender\Plugin\EntityTypeExtender;

use Drupal\entity_extender\Plugin\EntityExtender\EntityLifecycleInterface;
use Drupal\entity_extender\Plugin\EntityExtender\EntityLifecycleTranslationInterface;

/**
 * The internal mapping of Drupal hooks and lifecycle events for extenders.
 *
 * @internal
 */
final class EntityHandlerStorageLifecycles {

  /**
   * Defines all possible lifecycle events.
   */
  public const LIFECYCLES = [
    EntityLifecycleInterface::class => [
      'create' => 'onCreate',
      'revision_create' => 'onCreateRevision',
      'revision_predelete' => 'onPreDeleteRevision',
      'revision_delete' => 'onPostDeleteRevision',
      'presave' => 'onPreSave',
      'insert' => 'onInsert',
      'update' => 'onUpdate',
      'predelete' => 'onPreDelete',
      'delete' => 'onPostDelete',
    ],
    EntityLifecycleTranslationInterface::class => [
      'translation_create' => 'onCreateTranslation',
      'translation_insert' => 'onInsertTranslation',
      'translation_delete' => 'onPostDeleteTranslation',
    ],
  ];

}

\assert(
  (static function () {
    $flat = \array_merge(...\array_values(EntityHandlerStorageLifecycles::LIFECYCLES));
    // Ensure there are no duplicates in subarrays.
    \assert(\count($flat) === \count(\array_unique($flat)));

    foreach (EntityHandlerStorageLifecycles::LIFECYCLES as $interface => $methods) {
      foreach ((new \ReflectionClass($interface))->getMethods() as $method) {
        // Ensure parameterless.
        \assert($method->getNumberOfRequiredParameters() === 0, $method->name);
        // Ensure defined in the mapping.
        \assert(\in_array($method->name, $methods, TRUE), $method->name);
      }
    }

    return TRUE;
  })(),
);
