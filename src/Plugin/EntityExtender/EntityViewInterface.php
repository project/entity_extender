<?php

declare(strict_types=1);

namespace Drupal\entity_extender\Plugin\EntityExtender;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;

/**
 * The interface to implement by the extender plugin.
 *
 * The purpose of this interface is to allow an extender to provide
 * additional renderable data for an entity.
 *
 * @see \Drupal\entity_extender\Plugin\EntityTypeExtender\EntityHandlerTraitView::alterBuild()
 */
interface EntityViewInterface extends EntityExtenderHandlerInterface {

  /**
   * Alters the renderable data of an entity.
   *
   * @param array $build
   *   The renderable data to modify.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
   *   The entity display.
   * @param string $view_mode
   *   The view mode an entity is to be rendered with.
   *
   * @see \Drupal\Core\Entity\EntityViewBuilder::alterBuild()
   */
  public function alterRenderableData(array &$build, EntityViewDisplayInterface $display, string $view_mode): void;

}
