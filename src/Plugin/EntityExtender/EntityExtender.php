<?php

declare(strict_types=1);

namespace Drupal\entity_extender\Plugin\EntityExtender;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\entity_extender\Component\Memo;
use Drupal\entity_extender\EntityExtenderManagerInterface;
use JetBrains\PhpStorm\Immutable;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The base entity extender.
 */
abstract class EntityExtender extends PluginBase implements CacheableDependencyInterface, \Stringable {

  use Memo;

  /**
   * The entity's storage handler.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  #[Immutable]
  public EntityStorageInterface $storage;

  /**
   * A logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   When the entity type doesn't exist.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   When the storage handler couldn't be loaded.
   */
  public function __construct(
    array $plugin,
    #[Immutable]
    public EntityInterface $entity,
    protected EntityExtenderManagerInterface $entityExtenderManager,
    TranslationInterface $string_translation,
    LoggerChannelFactoryInterface $logger_factory,
  ) {
    \assert(!empty($plugin['id']));
    parent::__construct([], $plugin['id'], $plugin);

    $this->stringTranslation = $string_translation;
    $this->storage = $this->entityExtenderManager->getStorage($plugin['entity_type'], $plugin['entity_bundle']);
    $this->logger = $logger_factory->get(\sprintf('extender:%s:%s', $this->pluginId, $this->id()));
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   When the entity type doesn't exist.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   When the storage handler couldn't be loaded.
   */
  public static function create(
    ContainerInterface $container,
    EntityExtenderManagerInterface $entity_extender_manager,
    EntityInterface $entity,
    array $plugin,
    ...$arguments,
  ): static {
    return new static(
      $plugin,
      $entity,
      $entity_extender_manager,
      $container->get('string_translation'),
      $container->get('logger.factory'),
      ...$arguments,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __toString(): string {
    return $this->label();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts(): array {
    return $this->entity->getCacheContexts();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge(): int {
    return $this->entity->getCacheMaxAge();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags(): array {
    return $this->entity->getCacheTags();
  }

  /**
   * {@inheritdoc}
   */
  public function id(): ?int {
    $id = $this->entity->id();

    return $id === NULL ? $id : (int) $id;
  }

  /**
   * {@inheritdoc}
   */
  public function uuid(): ?string {
    return $this->entity->uuid();
  }

  /**
   * {@inheritdoc}
   */
  public function bundle(): string {
    return $this->entity->bundle();
  }

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    return (string) $this->entity->label();
  }

  /**
   * Returns the state of the operation.
   *
   * @return int|false
   *   The {@see \SAVED_NEW} or {@see \SAVED_UPDATED} depending on the operation
   *   performed. `false` can be returned on saving an existing, non-default
   *   revision {@see \Drupal\Core\Entity\ContentEntityStorageBase::doSave()}.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   When the entity cannot be saved.
   */
  public function save(): int|false {
    return $this->storage->save($this->entity);
  }

  /**
   * Returns the list of referenced entities within a field.
   *
   * @param string $field_name
   *   The name of the entity reference field.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   The list of referenced entities within a field.
   *
   * @throws \InvalidArgumentException
   *   When the field name is invalid.
   */
  protected function getReferencedEntities(string $field_name): array {
    return static::useMemo(
      function () use ($field_name): array {
        $list = $this->getList($field_name);

        if ($list === NULL) {
          $this->logger->warning('The "@uuid" is not a fieldable entity so cannot have references.', [
            '@uuid' => $this->uuid(),
          ]);

          return [];
        }

        if (!$list instanceof EntityReferenceFieldItemListInterface) {
          $this->logger->warning('The "@field_name" is not a reference field of "@uuid" entity.', [
            '@field_name' => $field_name,
            '@uuid' => $this->uuid(),
          ]);

          return [];
        }

        return $list->referencedEntities();
      },
      $this,
      __METHOD__,
      $field_name,
    );
  }

  /**
   * Returns the items list of a field.
   *
   * @param string $field_name
   *   The name of a field.
   * @param string|null $constraint
   *   The name of a constraint to assert on a field.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface|null
   *   The items list of a field.
   *
   * @throws \InvalidArgumentException
   *   When the field name is invalid.
   */
  protected function getList(string $field_name, string $constraint = NULL): ?FieldItemListInterface {
    // Do no check for `hasField` here because it becomes harder to understand:
    // why this method returned `null` - because of missing field or missing
    // value. In case field is missing exception will be thrown.
    if ($this->entity instanceof FieldableEntityInterface) {
      // Do not use `$field->getConstraint()` since there's an `isset()` inside
      // that doesn't work when a constraint has `null` as a configuration.
      /* @noinspection NullPointerExceptionInspection */
      \assert($constraint === NULL || \array_key_exists($constraint, $this->entity->getFieldDefinition($field_name)->getConstraints()));

      return $this->entity->get($field_name);
    }

    return NULL;
  }

  /**
   * Returns the value of a field.
   *
   * @param string $field_name
   *   The name of a field.
   * @param string|null $constraint
   *   The name of a constraint to assert on a field.
   *
   * @return mixed
   *   The value of a field.
   *
   * @throws \InvalidArgumentException
   *   When the field name is invalid.
   */
  protected function get(string $field_name, string $constraint = NULL) {
    return ($list = $this->getList($field_name, $constraint)) ? $list->value : NULL;
  }

}
