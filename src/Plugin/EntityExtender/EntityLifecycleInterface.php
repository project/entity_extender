<?php

declare(strict_types=1);

namespace Drupal\entity_extender\Plugin\EntityExtender;

/**
 * The interface to implement by the extender plugin.
 *
 * The purpose of this interface is to allow an extender to react
 * on entity lifecycle events.
 *
 * @see \Drupal\entity_extender\Plugin\EntityTypeExtender\EntityHandlerTraitStorage::lifecycle()
 */
interface EntityLifecycleInterface extends EntityExtenderHandlerInterface {

  /**
   * Reacts on the entity creation.
   */
  public function onCreate(): void;

  /**
   * Reacts on the entity revision creation.
   */
  public function onCreateRevision(): void;

  /**
   * Reacts on the attempt to save an entity.
   *
   * @throws \Exception
   *   When a save should be prevented.
   */
  public function onPreSave(): void;

  /**
   * Triggers after an entity was inserted.
   */
  public function onInsert(): void;

  /**
   * Triggers after an entity was updated.
   */
  public function onUpdate(): void;

  /**
   * Reacts on attempt to delete an entity.
   *
   * @throws \Exception
   *   When a deletion should be prevented.
   */
  public function onPreDelete(): void;

  /**
   * Triggers after an entity was deleted.
   */
  public function onPostDelete(): void;

  /**
   * Reacts on the attempt to remove a revision.
   *
   * @throws \Exception
   *   When a deletion should be prevented.
   */
  public function onPreDeleteRevision(): void;

  /**
   * Triggers after a revision was deleted.
   */
  public function onPostDeleteRevision(): void;

}
