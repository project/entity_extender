<?php

declare(strict_types=1);

namespace Drupal\entity_extender\Plugin\EntityExtender;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * The interface to implement by the extender plugin.
 *
 * The purpose of this interface is to define extra access frontier before the
 * access checks of Drupal core.
 *
 * @see \Drupal\entity_extender\Plugin\EntityTypeExtender\EntityHandlerTraitAccess::access()
 */
interface EntityAccessInterface extends EntityExtenderHandlerInterface {

  /**
   * Checks access to an operation on a given entity.
   *
   * @param string $operation
   *   The operation access should be checked for. Usually
   *   one of `view`, `view label`, `update` or `delete`.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account to check access for.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   *
   * @see \Drupal\Core\Entity\EntityAccessControlHandlerInterface::access()
   */
  public function access(string $operation, AccountInterface $account): AccessResultInterface;

}
