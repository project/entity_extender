<?php

declare(strict_types=1);

namespace Drupal\entity_extender\Plugin\EntityExtender;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * The interface to implement by the extender plugin.
 *
 * The purpose of this interface is to define extra access frontier before the
 * access checks of Drupal core.
 *
 * @see \Drupal\entity_extender\Plugin\EntityTypeExtender\EntityHandlerTraitAccess::fieldAccess()
 */
interface EntityFieldAccessInterface extends EntityExtenderHandlerInterface {

  /**
   * Checks access to an operation on a given field.
   *
   * @param string $operation
   *   The operation access should be checked for. Usually
   *   one of `view` or `edit`.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account to check access for.
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The field values for which to check access.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   *
   * @see \Drupal\Core\Entity\EntityAccessControlHandlerInterface::fieldAccess()
   */
  public function fieldAccess(
    string $operation,
    AccountInterface $account,
    FieldItemListInterface $items,
    FieldDefinitionInterface $field_definition,
  ): AccessResultInterface;

}
