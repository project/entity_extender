<?php

declare(strict_types=1);

namespace Drupal\entity_extender\Plugin\EntityExtender;

/**
 * The base interface to recognize extenders that implements entity handling.
 */
interface EntityExtenderHandlerInterface {}
