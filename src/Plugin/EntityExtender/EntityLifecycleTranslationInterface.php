<?php

declare(strict_types=1);

namespace Drupal\entity_extender\Plugin\EntityExtender;

/**
 * The interface to implement by the extender plugin.
 *
 * The purpose of this interface is to allow an extender to react
 * on entity lifecycle events.
 *
 * @see \Drupal\entity_extender\Plugin\EntityTypeExtender\EntityHandlerTraitStorage::lifecycle()
 */
interface EntityLifecycleTranslationInterface extends EntityExtenderHandlerInterface {

  /**
   * Reacts on the translation creation.
   *
   * Note: {@see \Drupal\Core\Entity\TranslatableInterface::addTranslation()}
   * causing this method to trigger immediately.
   *
   * @see \Drupal\Core\Entity\TranslatableInterface::addTranslation()
   * @see \Drupal\Core\Entity\TranslatableStorageInterface::createTranslation()
   * @see \Drupal\Core\Entity\ContentEntityStorageBase::createTranslation()
   */
  public function onCreateTranslation(): void;

  /**
   * Triggers after a translation was inserted.
   *
   * Note: {@see \Drupal\Core\Entity\TranslatableInterface::addTranslation()}
   * isn't causing a trigger of this method, and you must call `save`.
   *
   * @see \Drupal\Core\Entity\ContentEntityStorageBase::doPostSave()
   * @see \Drupal\Core\Entity\ContentEntityStorageBase::invokeTranslationHooks()
   */
  public function onInsertTranslation(): void;

  /**
   * Triggers after a translation was deleted.
   *
   * Note: {@see \Drupal\Core\Entity\TranslatableInterface::removeTranslation()}
   * isn't causing a trigger of this method, and you must call `save`.
   *
   * @see \Drupal\Core\Entity\ContentEntityStorageBase::doPostSave()
   * @see \Drupal\Core\Entity\ContentEntityStorageBase::invokeTranslationHooks()
   */
  public function onPostDeleteTranslation(): void;

}
