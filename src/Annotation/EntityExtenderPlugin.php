<?php

declare(strict_types=1);

namespace Drupal\entity_extender\Annotation;

use Drupal\Component\Annotation\PluginID;

/**
 * The annotation for defining entity extenders.
 *
 * @Annotation
 */
class EntityExtenderPlugin extends PluginID {

  /**
   * {@inheritdoc}
   */
  public function get(): array {
    $info = parent::get();

    \assert(\str_contains($info['id'], ':'));
    [$info['entity_type'], $info['entity_bundle']] = \explode(':', $info['id'], 2);

    return $info;
  }

}
