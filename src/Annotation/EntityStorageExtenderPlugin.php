<?php

declare(strict_types=1);

namespace Drupal\entity_extender\Annotation;

/**
 * The annotation for defining entity storage extenders.
 *
 * @Annotation
 */
class EntityStorageExtenderPlugin extends EntityExtenderPlugin {}
