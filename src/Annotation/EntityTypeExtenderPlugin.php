<?php

declare(strict_types=1);

namespace Drupal\entity_extender\Annotation;

use Drupal\Component\Annotation\PluginID;

/**
 * The annotation for defining entity types that can be extended.
 *
 * @Annotation
 */
class EntityTypeExtenderPlugin extends PluginID {}
