<?php

declare(strict_types=1);

namespace Drupal\entity_extender;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\entity_extender\Plugin\EntityExtender\EntityExtender;

/**
 * Describes the entity extender manager.
 */
interface EntityExtenderManagerInterface extends PluginManagerInterface {

  /**
   * Returns an instance of the "entity_type.manager" service.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   An instance of the "entity_type.manager" service.
   */
  public function getEntityTypeManager(): EntityTypeManagerInterface;

  /**
   * Returns the entity storage.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $bundle
   *   The entity bundle.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   The entity storage.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   When the entity type doesn't exist.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   When the storage handler couldn't be loaded.
   */
  public function getStorage(string $entity_type_id, string $bundle): EntityStorageInterface;

  /**
   * Returns the entity extender.
   *
   * @template T of EntityExtender
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to create an extender for.
   * @param class-string<T>|null $class
   *   The fully-qualified class name to create an instance of. It must extend
   *   the {@see EntityExtender}. Otherwise, the exception will be thrown.
   * @param mixed ...$arguments
   *   The arbitrary set of arguments to pass to the extender's constructor.
   *
   * @return T|null
   *   The entity extender.
   */
  public function getExtender(EntityInterface $entity, string $class = NULL, mixed ...$arguments): ?EntityExtender;

  /**
   * Returns the entity extender if it's a member of the class/interface.
   *
   * @template T of EntityExtender
   *
   * @param class-string $spec
   *   The interface/class the extender must implement/extend.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to create an extender for.
   * @param class-string<T>|null $class
   *   The fully-qualified class name to create an instance of. It must extend
   *   the {@see EntityExtender}. Otherwise, the exception will be thrown.
   * @param mixed ...$arguments
   *   The arbitrary set of arguments to pass to the extender's constructor.
   *
   * @return T|null
   *   The entity extender.
   */
  public function getExtenderIfMemberOf(string $spec, EntityInterface $entity, string $class = NULL, mixed ...$arguments): ?EntityExtender;

}
