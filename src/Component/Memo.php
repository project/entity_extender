<?php

declare(strict_types=1);

namespace Drupal\entity_extender\Component;

use Drupal\Component\Utility\Random;
use Drupal\Component\Uuid\Php as Uuid;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity_extender\Plugin\EntityExtender\EntityExtender;

/**
 * Returns the in-RAM storage.
 *
 * @return array
 *   The in-RAM storage.
 *
 * @internal
 */
function &storage(): array {
  static $storage = [];

  return $storage;
}

/**
 * Returns the ID of an entity.
 *
 * @param mixed $input
 *   The object to get an ID of.
 *
 * @return string|null
 *   The ID of an object.
 *
 * @internal
 */
function get_entity_id(mixed $input): ?string {
  if ($input instanceof EntityExtender) {
    $input = $input->entity;
  }

  // Identify accounts/users by email since the `AccountInterface` has
  // no `uuid()` method. Also remember that `UserInterface` extends
  // both `AccountInterface` and `EntityInterface`.
  if ($input instanceof AccountInterface) {
    return $input->getEmail() ?: \sprintf('%s@place.holder', (new Random)->name(8, TRUE));
  }

  if ($input instanceof EntityInterface) {
    $uuid = $input->uuid() ?: (new Uuid)->generate();

    // UUID is the same across all revisions of a specific entity.
    if ($input instanceof RevisionableInterface && $revision_id = $input->getRevisionId()) {
      $uuid .= '_' . $revision_id;
    }

    // The same is applicable for translations - they share a single UUID.
    if ($input instanceof TranslatableInterface) {
      $uuid .= '_' . $input->language()->getId();
    }

    return $uuid;
  }

  return NULL;
}

/**
 * The in-RAM static cache for the expensive computations.
 *
 * @link https://en.wikipedia.org/wiki/Memoization
 */
trait Memo {

  /**
   * Returns the memoized result of a callable.
   *
   * @param \Closure $setter
   *   The callback to produce a value for memoization.
   * @param mixed ...$inputs
   *   The list of input parameters to associate the result with.
   *
   * @return mixed
   *   The memoized result.
   */
  public static function useMemo(\Closure $setter, mixed ...$inputs): mixed {
    $trace = \debug_backtrace(\DEBUG_BACKTRACE_IGNORE_ARGS, 2);
    // The local or trait-level static variable is not an option here because
    // for each `use` of this trait will be allocated the own storage in RAM.
    $storage =& storage();
    // Use the caller and the line of where `useMemo` is called to prevent
    // collisions.
    $pointer = \array_map(static fn (array $item): string => $item['file'] . ':' . $item['line'], $trace);
    $entities = [];

    foreach ($inputs as $input) {
      if ($id = get_entity_id($input)) {
        $entities[] = $id;
      }
      // Allow passing an instance of the `ReflectionMethod` in order to
      // memoize a value for instances where the method is not overridden.
      elseif ($input instanceof \ReflectionMethod) {
        $pointer[] = $input->getDeclaringClass()->getName();
      }
      elseif (\is_scalar($input)) {
        $pointer[] = $input;
      }
      // Skip empty inputs and throw for arrays, objects, resources etc.
      elseif (!empty($input)) {
        throw new \InvalidArgumentException(\sprintf('The "%s" type is not supported.', \gettype($input)));
      }
    }

    // Prepend entity IDs to the pointer.
    $pointer = \array_merge($entities, $pointer);
    \assert(\count($pointer) > 2);
    $pointer = \md5(\implode('--', $pointer));

    // Store mapping of entity IDs and cache pointer.
    foreach ($entities as $id) {
      $storage['mapping'][$id][$pointer] = TRUE;
    }

    if (!\array_key_exists($pointer, $storage)) {
      $storage[$pointer] = $setter();
    }

    return $storage[$pointer];
  }

  /**
   * Wipes out memoized values of computations where an entity object was used.
   *
   * @param \Drupal\Core\Session\AccountInterface|\Drupal\Core\Entity\EntityInterface|\Drupal\entity_extender\Plugin\EntityExtender\EntityExtender $input
   *   The object to invalidate the memoized values of computations where
   *   it was used.
   *
   * @see \entity_extender_entity_update()
   */
  public static function undoMemo(AccountInterface|EntityInterface|EntityExtender $input): void {
    if ($id = get_entity_id($input)) {
      $storage =& storage();

      foreach ($storage['mapping'][$id] ?? [] as $pointer => $noop) {
        // Remove the data and associated cache pointer.
        unset($storage[$pointer], $storage['mapping'][$id][$pointer]);
      }
    }
  }

}
