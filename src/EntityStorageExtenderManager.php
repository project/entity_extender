<?php

declare(strict_types=1);

namespace Drupal\entity_extender;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\entity_extender\Annotation\EntityStorageExtenderPlugin;

/**
 * The entity storage extender manager.
 */
class EntityStorageExtenderManager extends DefaultPluginManager implements EntityStorageExtenderManagerInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler,
  ) {
    parent::__construct(
      'Plugin/EntityStorageExtender',
      $namespaces,
      $module_handler,
      EntityStorageInterface::class,
      EntityStorageExtenderPlugin::class,
    );

    $this->setCacheBackend($cache_backend, $this->subdir);
  }

  /**
   * {@inheritdoc}
   */
  final public function createInstance($plugin_id, array $configuration = []): void {
    throw new \BadMethodCallException("This API is not supported - use `\Drupal::service('entity_extender.manager')->getStorage()`.");
  }

  /**
   * {@inheritdoc}
   */
  final public function getInstance(array $options): void {
    throw new \BadMethodCallException("This API is not supported - use `\Drupal::service('entity_extender.manager')->getStorage()`.");
  }

}
