<?php

declare(strict_types=1);

// phpcs:disable Drupal.Classes.ClassFileName.NoMatch

namespace Drupal\Tests\entity_extender\Unit;

use Drupal\Component\Annotation\PluginID;
use Drupal\Component\Plugin\Discovery\DiscoveryInterface;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\entity_extender\Annotation\EntityExtenderPlugin;
use Drupal\entity_extender\Annotation\EntityStorageExtenderPlugin;
use Drupal\entity_extender\Annotation\EntityTypeExtenderPlugin;
use Drupal\entity_extender\Component\Memo;
use Drupal\entity_extender\EntityExtenderManager;
use Drupal\entity_extender\EntityExtenderManagerInterface;
use Drupal\entity_extender\EntityStorageExtenderManager;
use Drupal\entity_extender\EntityTypeExtenderManager;
use Drupal\entity_extender\EntityTypeExtenderManagerInterface;
use Drupal\entity_extender\Plugin\EntityExtender\EntityAccessInterface;
use Drupal\entity_extender\Plugin\EntityExtender\EntityExtender;
use Drupal\entity_extender\Plugin\EntityExtender\EntityFieldAccessInterface;
use Drupal\entity_extender_user\Plugin\EntityTypeExtender\UserEntityTypeExtender;
use Drupal\Tests\UnitTestCase;
use Drupal\user\UserInterface;
use Drupal\user\UserStorage;
use Drupal\user\UserStorageInterface;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * The default user extender mock.
 */
class UserExtenderDefault extends EntityExtender {

  /**
   * {@inheritdoc}
   */
  public function getFullName(): string {
    return 'Full Name';
  }

}

/**
 * The user extender mock providing additional functionality.
 */
class UserExtenderAccess extends UserExtenderDefault implements EntityAccessInterface, EntityFieldAccessInterface {

  /**
   * {@inheritdoc}
   */
  public function access(string $operation, AccountInterface $account): AccessResultInterface {
    return new AccessResultAllowed();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldAccess(string $operation, AccountInterface $account, FieldItemListInterface $items, FieldDefinitionInterface $field_definition): AccessResultInterface {
    return $this->access("field-$operation", $account);
  }

}

/**
 * The user extender mock with a custom argument.
 */
class UserExtenderCustomArg extends UserExtenderDefault {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $plugin,
    EntityInterface $entity,
    EntityExtenderManagerInterface $entity_extender_manager,
    TranslationInterface $string_translation,
    LoggerChannelFactoryInterface $logger_factory,
    protected int $number,
  ) {
    parent::__construct($plugin, $entity, $entity_extender_manager, $string_translation, $logger_factory);
  }

  /**
   * {@inheritdoc}
   */
  public function getNumber(): int {
    return $this->number;
  }

}

/**
 * The user storage extender plugin.
 */
class UserStorageExtender extends UserStorage {
}

/**
 * Tests module API.
 *
 * @group entity_extender
 */
class EntityExtenderUnitTest extends UnitTestCase {

  use Memo;

  /**
   * Tests the entity class is checked for misimplementations.
   *
   * @param class-string $interface
   *   The interface to check the implementation of.
   *
   * @dataProvider providerIsFlawless
   */
  public function testIsFlawless(string $interface): void {
    // @todo Replace for `expectWarning` when available.
    $this->expectWarning();
    $this->expectExceptionMessage(\sprintf(
      'The "%s" is implemented by "%s" however there are no entity type extenders to provide functionality for it.',
      // @todo PHPUnit throws an exception on warning and this doesn't let the program to continue and blocks asserting further warnings.
      // @todo We must use the `$interface` here!
      EntityAccessInterface::class,
      UserExtenderAccess::class,
    ));

    $account = $this->getEntity(UserInterface::class, [
      'getEntityTypeId' => ['user', 1],
      'bundle' => ['user', 1],
    ]);

    static::assertTrue(\assert($account instanceof UserInterface));
    static::assertTrue(\is_subclass_of(UserExtenderAccess::class, $interface));

    $this
      ->getEntityExtenderManager(1, 0, [])
      ->getExtender($account, UserExtenderAccess::class);
  }

  /**
   * Returns the test cases.
   *
   * @return array
   *   The test cases.
   */
  public function providerIsFlawless(): array {
    return [
      [EntityAccessInterface::class],
      [EntityFieldAccessInterface::class],
    ];
  }

  /**
   * Tests entity extender manager.
   */
  public function testEntityExtenderManagerGetExtender(): void {
    $user_id = 1;
    $plugin_type_extender = $this->getEntityTypeExtenderPlugin('user', UserEntityTypeExtender::class);
    $entity_extender_manager = $this->getEntityExtenderManager($user_id, 3, [$plugin_type_extender], []);
    $account = $this->getEntity(UserInterface::class, [
      'getEntityTypeId' => ['user', 5],
      'bundle' => ['user', 5],
      'id' => [$user_id, 3],
    ]);

    static::assertTrue(\assert($account instanceof UserInterface));

    $entity_type_manager = $entity_extender_manager->getEntityTypeManager();
    static::assertTrue(\assert($entity_type_manager instanceof MockObject));
    $user_storage = $this->createMock(UserStorageInterface::class);
    $entity_type_manager
      ->expects(static::exactly(3))
      ->method('getStorage')
      ->with('user')
      ->willReturn($user_storage);

    $extender = $entity_extender_manager->getExtender($account);
    static::assertTrue(\assert($extender instanceof UserExtenderDefault));
    static::assertSame('Full Name', $extender->getFullName());
    static::assertSame($account, $extender->entity);

    $extender = $entity_extender_manager->getExtender($account, UserExtenderCustomArg::class, 12);
    static::assertTrue(\assert($extender instanceof UserExtenderCustomArg));
    static::assertSame('Full Name', $extender->getFullName());
    static::assertSame($account, $extender->entity);
    static::assertSame(12, $extender->getNumber());

    // Test passing the extender as an argument to Memo.
    static::assertSame(1, static::useMemo(static fn (): int => 1, $extender));

    try {
      $entity_extender_manager->getExtender($account, \stdClass::class);
      static::fail(\sprintf('The "%s" is not a valid class for the extender.', \stdClass::class));
    }
    catch (\InvalidArgumentException $e) {
      static::assertSame(
        \sprintf('The "%s" for instantiating "user:user" must be a child of "%s".', \stdClass::class, EntityExtender::class),
        $e->getMessage(),
      );
    }

    try {
      $entity_extender_manager->getDefinition('wrong-name');
      static::fail('The "wrong-name" definition must not exist.');
    }
    catch (PluginNotFoundException $e) {
      static::assertSame(
        \sprintf('The "wrong-name" plugin does not exist. Valid plugin IDs for %s are: user:user', $entity_extender_manager::class),
        $e->getMessage(),
      );
    }

    static::assertNull(
      $entity_extender_manager->getExtenderIfMemberOf(EntityAccessInterface::class, $account),
      'The default plugin class does not implement this interface.',
    );

    static::assertNotEmpty(
      $entity_extender_manager->getDefinition(UserExtenderDefault::class, FALSE),
      'Ensure a plugin definition can be gotten by passing a plugin class instead of ID.',
    );

    $extender = $entity_extender_manager->getExtenderIfMemberOf(EntityAccessInterface::class, $account, UserExtenderAccess::class);
    static::assertTrue(\assert($extender instanceof UserExtenderAccess));

    $entity = $this->getEntity(EntityInterface::class, [
      'getEntityTypeId' => ['custom_type', 1],
      'bundle' => ['article', 1],
    ]);

    static::assertTrue(\assert($entity instanceof EntityInterface));
    static::assertNull($entity_extender_manager->getExtender($entity));
    static::assertNull($entity_extender_manager->getDefinition('non-existent', FALSE));
  }

  /**
   * Tests entity extender manager.
   */
  public function testEntityExtenderManagerGetStorage(): void {
    $entity_type_id = 'user';
    $entity_bundle = 'user';
    $entity_id = 1;
    $plugin_type_storage_extender = $this->getEntityStorageExtenderPlugin("$entity_type_id:$entity_bundle", UserStorageExtender::class);
    $entity_extender_manager = $this->getEntityExtenderManager($entity_id, 1, NULL, [$plugin_type_storage_extender]);
    $entity_type_manager = $entity_extender_manager->getEntityTypeManager();
    $user_entity_type = $this->createMock(ContentEntityType::class);
    $user_storage_extender = $this->createMock(UserStorageExtender::class);
    $user_storage_extender_invalid = $this->createMock(EntityStorageInterface::class);
    // phpcs:ignore Drupal.Arrays.Array.LongLineDeclaration
    $user_storage_extenders = [new \stdClass(), $user_storage_extender_invalid, $user_storage_extender, $user_storage_extender];
    $user_storage_invocation_rule = static::exactly(\count($user_storage_extenders));

    static::assertTrue(\assert($entity_type_manager instanceof MockObject));
    $entity_type_manager
      // +1 repeated call to test memo.
      ->expects(static::exactly(\count($user_storage_extenders) + 1))
      ->method('getDefinition')
      ->with($entity_type_id)
      ->willReturn($user_entity_type);

    try {
      $entity_extender_manager->getStorage($entity_type_id, $entity_bundle);
      static::fail('The storage extender must not be created.');
    }
    catch (InvalidPluginDefinitionException $e) {
      static::assertSame('The "user" entity type did not specify a storage handler.', $e->getMessage());
    }

    $user_entity_type
      ->expects(clone $user_storage_invocation_rule)
      ->method('getHandlerClass')
      ->with('storage')
      ->willReturn(UserStorage::class);

    $entity_type_manager
      ->expects(clone $user_storage_invocation_rule)
      ->method('createHandlerInstance')
      ->with(UserStorageExtender::class, $user_entity_type)
      ->willReturnOnConsecutiveCalls(...$user_storage_extenders);

    try {
      $entity_extender_manager->getStorage($entity_type_id, $entity_bundle);
      static::fail('The storage extender is invalid.');
    }
    catch (\AssertionError $e) {
      static::assertSame($plugin_type_storage_extender->getId(), $e->getMessage());
    }

    try {
      $entity_extender_manager->getStorage($entity_type_id, $entity_bundle);
      static::fail('The storage extender is invalid.');
    }
    catch (InvalidPluginDefinitionException $e) {
      static::assertSame(
        \sprintf('The "%s" must be a child of "%s".', $user_storage_extender_invalid::class, UserStorage::class),
        $e->getMessage(),
      );
    }

    // Test memoization.
    for ($i = 0; $i < 2; $i++) {
      // We call the operation in a loop because the memoization uses the
      // file and line of the callee to create the unique pointer to the value.
      static::assertSame($user_storage_extender, $entity_extender_manager->getStorage($entity_type_id, $entity_bundle));
    }

    $account = $this->getEntity(UserInterface::class, [
      'getEntityTypeId' => [$entity_type_id, 1],
      'bundle' => [$entity_bundle, 1],
      'id' => [$entity_id, 1],
    ]);
    static::assertTrue(\assert($account instanceof UserInterface));
    $account_extender = $entity_extender_manager->getExtender($account);
    static::assertSame($account, $account_extender->entity);
    static::assertSame($user_storage_extender, $account_extender->storage);
  }

  /**
   * Returns the mock of the entity extender manager.
   *
   * @param int $user_id
   *   The user ID.
   * @param int $calls_ok
   *   The number of expected successful extenders' instances.
   * @param \Drupal\entity_extender\Annotation\EntityTypeExtenderPlugin[]|null $definitions_type_extenders
   *   The list of entity type extenders' plugins.
   * @param \Drupal\entity_extender\Annotation\EntityStorageExtenderPlugin[]|null $definitions_storage_extenders
   *   The list of entity storage extenders' plugins.
   *
   * @return \PHPUnit\Framework\MockObject\MockObject&\Drupal\entity_extender\EntityExtenderManagerInterface
   *   The mock of the entity extender manager.
   */
  protected function getEntityExtenderManager(
    int $user_id,
    int $calls_ok,
    array $definitions_type_extenders = NULL,
    array $definitions_storage_extenders = NULL,
  ): EntityExtenderManagerInterface {
    $plugin = $this->getEntityExtenderPlugin('user:user', UserExtenderDefault::class);
    $namespaces = new \ArrayObject();
    $cache_backend = $this->createMock(CacheBackendInterface::class);
    $logger_factory = $this->createMock(LoggerChannelFactoryInterface::class);
    $module_handler = $this->createMock(ModuleHandlerInterface::class);
    $entity_type_manager = $this->createMock(EntityTypeManagerInterface::class);
    $entity_storage_extender_manager = $this->getPluginManager(
      EntityStorageExtenderManager::class,
      $definitions_storage_extenders,
      [
        $namespaces,
        $cache_backend,
        $module_handler,
      ],
    );
    $entity_type_extender_manager = $this->getPluginManager(
      EntityTypeExtenderManager::class,
      $definitions_type_extenders,
      [
        $namespaces,
        $cache_backend,
        $module_handler,
      ],
    );
    $entity_extender_manager = $this->getPluginManager(
      EntityExtenderManager::class,
      [$plugin],
      [
        $namespaces,
        $cache_backend,
        $module_handler,
        $entity_type_manager,
        $entity_type_extender_manager,
        $entity_storage_extender_manager,
      ],
    );

    $logger_factory
      ->expects(static::exactly($calls_ok))
      ->method('get')
      ->with(\sprintf('extender:%s:%s', $plugin->getId(), $user_id))
      ->willReturn($this->createMock(LoggerChannelInterface::class));

    // Define the container and dependencies of the EntityExtender base class.
    /* @see \Drupal\entity_extender\Plugin\EntityExtender\EntityExtender::create() */
    $container = new ContainerBuilder();
    $container->set('logger.factory', $logger_factory);
    $container->set('string_translation', $this->createMock(TranslationInterface::class));
    \Drupal::setContainer($container);

    static::assertTrue(\assert($entity_extender_manager instanceof EntityExtenderManagerInterface));
    static::assertTrue(\assert($entity_type_extender_manager instanceof EntityTypeExtenderManagerInterface));
    static::assertSame($entity_type_manager, $entity_extender_manager->getEntityTypeManager());

    // These plugin managers cannot create instances using the standard API.
    foreach ([
      [$entity_extender_manager, 'getExtender'],
      [$entity_storage_extender_manager, 'getStorage'],
    ] as [$plugin_manager, $replacer]) {
      foreach (['createInstance' => ['wrong-name'], 'getInstance' => [[]]] as $method => $args) {
        try {
          $plugin_manager->{$method}(...$args);
          static::fail(\sprintf('The "%s()" must not be allowed for use.', $method));
        }
        catch (\BadMethodCallException $e) {
          static::assertSame(
            "This API is not supported - use `\Drupal::service('entity_extender.manager')->$replacer()`.",
            $e->getMessage(),
          );
        }
      }
    }

    return $entity_extender_manager;
  }

  /**
   * Returns the mock of a plugin manager.
   *
   * @param class-string<\Drupal\Component\Plugin\PluginManagerInterface> $class
   *   The plugin manager class.
   * @param \Drupal\Component\Annotation\PluginID[]|null $plugins
   *   The list of plugins.
   * @param array $arguments
   *   The list of manager's arguments.
   *
   * @return \PHPUnit\Framework\MockObject\MockObject
   *   The mock of a plugin manager.
   */
  protected function getPluginManager(string $class, ?array $plugins, array $arguments): MockObject {
    static::assertTrue(\is_subclass_of($class, PluginManagerInterface::class), $class);
    $definitions = [];

    if ($plugins === NULL) {
      $invocation_rule = static::never();
    }
    else {
      $invocation_rule = static::once();

      foreach ($plugins as $i => $plugin) {
        static::assertTrue(\assert($plugin instanceof PluginID), (string) $i);
        $definitions[$plugin->getId()] = $plugin->get();
      }
    }

    $instance = $this
      ->getMockBuilder($class)
      ->setConstructorArgs($arguments)
      ->onlyMethods(['getDiscovery'])
      ->getMock();

    $discovery = $this->createMock(DiscoveryInterface::class);

    $discovery
      ->expects(clone $invocation_rule)
      ->method('getDefinitions')
      ->willReturn($definitions);

    $instance
      ->expects(clone $invocation_rule)
      ->method('getDiscovery')
      ->willReturn($discovery);

    return $instance;
  }

  /**
   * Returns the entity mock.
   *
   * @param string $interface
   *   The interface to create a mock of.
   * @param array $specs
   *   The method calls specifications.
   *
   * @return \PHPUnit\Framework\MockObject\MockObject
   *   The entity mock.
   */
  protected function getEntity(string $interface, array $specs): MockObject {
    $entity = $this->createMock($interface);

    foreach ($specs as $method => [$return, $calls]) {
      $entity
        ->expects(static::exactly($calls))
        ->method($method)
        ->willReturn($return);
    }

    return $entity;
  }

  /**
   * Returns an instance of the annotation.
   *
   * @template T of PluginID
   *
   * @param class-string<T> $annotation
   *   The annotation class.
   * @param string $id
   *   The plugin ID.
   * @param string $class
   *   The plugin class.
   *
   * @return T
   *   An instance of the annotation.
   */
  protected static function getAnnotation(string $annotation, string $id, string $class): PluginID {
    $plugin = new $annotation();
    static::assertTrue(\assert($plugin instanceof PluginID));
    $plugin->value = $id;
    $plugin->setClass($class);

    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityTypeExtenderPlugin(string $id, string $class): EntityTypeExtenderPlugin {
    return static::getAnnotation(EntityTypeExtenderPlugin::class, $id, $class);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityStorageExtenderPlugin(string $id, string $class): EntityStorageExtenderPlugin {
    return static::getAnnotation(EntityStorageExtenderPlugin::class, $id, $class);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityExtenderPlugin(string $id, string $class): EntityExtenderPlugin {
    return static::getAnnotation(EntityExtenderPlugin::class, $id, $class);
  }

}
