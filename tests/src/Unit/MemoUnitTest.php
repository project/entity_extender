<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_extender\Unit;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity_extender\Component\Memo;
use Drupal\taxonomy\Entity\Term;
use Drupal\Tests\UnitTestCase;

/**
 * Tests memoization.
 *
 * @group entity_extender
 */
class MemoUnitTest extends UnitTestCase {

  use Memo;

  /**
   * {@inheritdoc}
   */
  protected function getEntity(int $calls): EntityInterface {
    $entity = $this->createMock(EntityInterface::class);
    $entity
      ->expects(static::exactly($calls))
      ->method('uuid')
      ->willReturn('1f70ad30-5608-4012-b719-a71d2ad6ea0d');

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  protected function getAccount(int $calls): AccountInterface {
    $account = $this->createMock(AccountInterface::class);
    $account
      ->expects(static::exactly($calls))
      ->method('getEmail')
      ->willReturn('test@example.com');

    return $account;
  }

  public function testRevisionableEntity(): void {
    $entities = [];

    $language = $this->createMock(LanguageInterface::class);
    $language
      ->method('getId')
      ->willReturn('en');

    foreach (['rev1', 'rev2'] as $i => $label) {
      $entity = $this->createMock(Term::class);

      $entity
        ->expects(static::once())
        ->method('label')
        ->willReturn($label);
      $entity
        ->method('getRevisionId')
        ->willReturn($i + 1);
      $entity
        ->method('uuid')
        // Revisions do have the UUID of the entity.
        // Stupid, but they really do.
        ->willReturn('1f70ad30-5608-4012-b719-a71d2ad6ea0d');
      $entity
        ->method('language')
        ->willReturn($language);

      $entities[$label] = $entity;
    }

    foreach ($entities as $label => $entity) {
      static::assertSame($label, static::useMemo(static fn () => $entity->label(), $entity));
    }
  }

  public function testTranslatableEntity(): void {
    $entities = [];

    foreach (['fr', 'de'] as $langcode) {
      $entity = $this->createMock(Term::class);
      $language = $this->createMock(LanguageInterface::class);

      $language
        ->method('getId')
        ->willReturn($langcode);
      $entity
        ->expects(static::once())
        ->method('label')
        ->willReturn($langcode);
      $entity
        ->method('uuid')
        // Translations do have the UUID of the entity.
        // Stupid, but they really do.
        ->willReturn('1f70ad30-5608-4012-b719-a71d2ad6ea0d');
      $entity
        ->method('language')
        ->willReturn($language);

      $entities[$langcode] = $entity;
    }

    foreach ($entities as $label => $entity) {
      static::assertSame($label, static::useMemo(static fn () => $entity->label(), $entity));
    }
  }

  /**
   * Tests input arguments.
   */
  public function testInputs(): void {
    $method = new \ReflectionMethod(static::class, __FUNCTION__);

    foreach ([1, 2] as $actual) {
      static::assertSame(1, static::useMemo(static fn (): int => $actual, $method));
      // Add dependency as an input.
      static::assertSame($actual, static::useMemo(static fn (): int => $actual, $actual, $method));
    }

    static::assertSame(1, static::useMemo(static fn (): int => 1, $method));
    // Even despite the inputs are the same the call is on a new line so
    // it's considered as a new one.
    static::assertSame(2, static::useMemo(static fn (): int => 2, $method));

    // Two calls on the same line produce a single result when the same
    // inputs are used.
    static::assertSame(1, static::useMemo(static fn (): int => 1, $method)); static::assertSame(1, static::useMemo(static fn (): int => 2, $method));
  }

  /**
   * Tests invalid argument.
   */
  public function testInvalidInput(): void {
    $this->expectException(\InvalidArgumentException::class);
    $this->expectExceptionMessage('The "object" type is not supported.');

    static::assertSame(2, static::useMemo(static fn (): int => 2, new \stdClass()));
  }

  /**
   * Tests memoization.
   *
   * @param bool $use_entity
   *   The state of whether to use an entity as an argument.
   * @param bool $use_account
   *   The state of whether to use an account as an argument.
   * @param \Throwable|null $exception
   *   The expected exception to be thrown during the use of a static cache.
   *
   * @dataProvider provider
   */
  public function test(bool $use_entity, bool $use_account, ?\Throwable $exception): void {
    if ($exception !== NULL) {
      $this->expectException($exception::class);
      $this->expectExceptionMessage($exception->getMessage());
    }

    $iterations_count = 5;
    $executions_count = 0;

    $entity = $use_entity ? $this->getEntity($iterations_count) : NULL;
    $account = $use_account ? $this->getAccount($iterations_count) : NULL;

    for ($iterations_passed = 0; $iterations_passed < $iterations_count; $iterations_passed++) {
      static::assertSame('bla', static::useMemo(static function () use (&$executions_count): string {
        $executions_count++;

        return 'bla';
      }, $entity, $account));
    }

    static::assertSame($iterations_count, $iterations_passed);
    static::assertSame(1, $executions_count);
  }

  /**
   * {@inheritdoc}
   */
  public function provider(): array {
    return [
      [FALSE, FALSE, new \AssertionError('assert(\count($pointer) > 2)')],
      [TRUE, FALSE, NULL],
      [FALSE, TRUE, NULL],
      [TRUE, TRUE, NULL],
    ];
  }

}
