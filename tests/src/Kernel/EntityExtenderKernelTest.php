<?php

declare(strict_types=1);

// phpcs:disable Drupal.Classes.ClassFileName.NoMatch

namespace Drupal\Tests\entity_extender\Kernel;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityViewBuilderInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\RevisionableStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity_extender\Access\AccessResultAllowedWithoutCoreAccessChecks;
use Drupal\entity_extender\Annotation\EntityExtenderPlugin;
use Drupal\entity_extender\Annotation\EntityTypeExtenderPlugin;
use Drupal\entity_extender\Plugin\EntityExtender\EntityAccessInterface;
use Drupal\entity_extender\Plugin\EntityExtender\EntityExtender;
use Drupal\entity_extender\Plugin\EntityExtender\EntityFieldAccessInterface;
use Drupal\entity_extender\Plugin\EntityExtender\EntityLifecycleInterface;
use Drupal\entity_extender\Plugin\EntityExtender\EntityLifecycleTranslationInterface;
use Drupal\entity_extender\Plugin\EntityExtender\EntityViewInterface;
use Drupal\entity_extender\Plugin\EntityTypeExtender\EntityTypeExtender;
use Drupal\KernelTests\KernelTestBase;
use Drupal\user\UserInterface;

/**
 * Test implementation of the entity extender.
 */
class TestEntityExtender extends EntityExtender {

  /**
   * Increments the method call counter.
   *
   * @param string $function
   *   The method name.
   */
  protected function increment(string $function): void {
    $GLOBALS[static::class][$function] ??= 0;
    $GLOBALS[static::class][$function]++;
  }

}

/**
 * Test implementation of the entity extender.
 */
class AccessEntityExtender extends TestEntityExtender implements EntityAccessInterface, EntityFieldAccessInterface {

  /**
   * {@inheritdoc}
   */
  public function access(string $operation, AccountInterface $account): AccessResultInterface {
    $this->increment($operation);

    return match ($operation) {
      'allowed-immediately' => new AccessResultAllowedWithoutCoreAccessChecks(),
      'forbidden' => AccessResult::forbidden(),
      'allowed' => AccessResult::allowed(),
      default => AccessResult::neutral(),
    };
  }

  /**
   * {@inheritdoc}
   */
  public function fieldAccess(string $operation, AccountInterface $account, FieldItemListInterface $items, FieldDefinitionInterface $field_definition): AccessResultInterface {
    // @todo Test this method!
    return $this->access("field-$operation", $account);
  }

}

/**
 * Test implementation of the entity extender.
 */
class LifecycleEntityExtender extends TestEntityExtender implements EntityLifecycleInterface, EntityLifecycleTranslationInterface {

  /**
   * {@inheritdoc}
   */
  public function onCreate(): void {
    $this->increment(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function onCreateRevision(): void {
    $this->increment(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function onPreSave(): void {
    $this->increment(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function onInsert(): void {
    $this->increment(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function onUpdate(): void {
    $this->increment(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function onPreDelete(): void {
    $this->increment(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function onPostDelete(): void {
    $this->increment(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function onPreDeleteRevision(): void {
    $this->increment(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function onPostDeleteRevision(): void {
    $this->increment(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function onCreateTranslation(): void {
    $this->increment(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function onInsertTranslation(): void {
    $this->increment(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function onPostDeleteTranslation(): void {
    $this->increment(__FUNCTION__);
  }

}

/**
 * Test implementation of the entity extender.
 */
class RenderableEntityExtender extends TestEntityExtender implements EntityViewInterface {

  /**
   * {@inheritdoc}
   */
  public function alterRenderableData(array &$build, EntityViewDisplayInterface $display, string $view_mode): void {
    $this->increment(__FUNCTION__);
  }

}

/**
 * Tests module API.
 *
 * @group entity_extender
 */
abstract class EntityExtenderKernelTest extends KernelTestBase {

  /**
   * The FQCN of the entity type extender.
   */
  protected const ENTITY_TYPE_EXTENDER = NULL;

  /**
   * The bundle for the test entity extender.
   *
   * NOTE: leave NULL if the same as entity type.
   */
  protected const ENTITY_EXTENDER_BUNDLE = NULL;

  /**
   * The minimum set of values needed for creating an entity.
   */
  protected const ENTITY_CREATE_DATA = [];

  /**
   * The minimum set of values to create the bundle entity.
   *
   * NOTE: applicable only if {@see ENTITY_EXTENDER_BUNDLE} is set.
   */
  protected const ENTITY_BUNDLE_CREATE_DATA = [];

  /**
   * The name of a module, providing the entity type extender.
   *
   * @var string
   */
  protected static string $provider;

  /**
   * The extended entity type ID.
   *
   * @var string
   */
  protected static string $entityType;

  /**
   * {@inheritdoc}
   */
  public function __construct($name = NULL, array $data = [], $data_name = '') {
    $annotation = \basename(\str_replace('\\', '/', EntityTypeExtenderPlugin::class));

    if (\preg_match('/@' . $annotation . '\("(.+?)"\)/', (new \ReflectionClass(static::ENTITY_TYPE_EXTENDER))->getDocComment(), $matches) !== 1) {
      throw new \LogicException(\sprintf('The "%s" is invalid entity type extender.', static::ENTITY_TYPE_EXTENDER));
    }

    static::$provider = \explode('\\', static::ENTITY_TYPE_EXTENDER)[1];
    static::$entityType = $matches[1];

    parent::__construct($name, $data, $data_name);
  }

  /**
   * Returns the entity type storage.
   *
   * @param string $extender_plugin_class
   *   The extender's plugin class.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   The entity type storage.
   *
   * @throws \Exception
   */
  protected function getConfiguredEntityStorage(string $extender_plugin_class): EntityStorageInterface {
    $entity_types = \array_unique(['user', static::$entityType]);

    static::assertTrue(
      $this->container
        ->get('module_installer')
        ->install(['system', ...$entity_types, static::$provider]),
    );

    \array_map([$this, 'installEntitySchema'], $entity_types);

    if (static::ENTITY_EXTENDER_BUNDLE !== NULL) {
      $entity_type_manager = $this->container->get('entity_type.manager');
      $entity_type = $entity_type_manager->getDefinition(static::$entityType);
      static::assertTrue(\assert($entity_type instanceof EntityTypeInterface));

      if ($bundle_entity_type_id = $entity_type->getBundleEntityType()) {
        $this->installEntitySchema($bundle_entity_type_id);
        $bundle_entity_storage = $entity_type_manager->getStorage($bundle_entity_type_id);
        static::assertTrue(\assert($bundle_entity_storage instanceof EntityStorageInterface));
        $bundle_entity_storage->save($bundle_entity_storage->create(static::ENTITY_BUNDLE_CREATE_DATA));
      }
    }

    $plugin = new EntityExtenderPlugin();
    $plugin->value = \sprintf('%s:%s', static::$entityType, static::ENTITY_EXTENDER_BUNDLE ?? static::$entityType);
    $plugin->setClass($extender_plugin_class);
    $plugin->setProvider(static::$provider);
    static::assertSame($plugin->value, $plugin->getId());
    static::assertSame(static::$provider, $plugin->getProvider());
    static::assertSame($extender_plugin_class, $plugin->getClass());

    $entity_extender_manager = $this->container->get('entity_extender.manager');
    $entity_storage = $entity_extender_manager->getEntityTypeManager()->getStorage(static::$entityType);
    static::assertTrue(\assert($entity_storage instanceof EntityStorageInterface));

    $definitions = new \ReflectionProperty($entity_extender_manager, 'definitions');
    $definitions->setAccessible(TRUE);
    $definitions->setValue($entity_extender_manager, [
      $plugin->value => $plugin->get(),
    ]);

    return $entity_storage;
  }

  /**
   * Tests render of the extender.
   *
   * @throws \Exception
   *
   * @see \Drupal\entity_extender\Plugin\EntityTypeExtender\EntityHandlerTraitView
   */
  public function testEntityHandlerTraitView(): void {
    $class = RenderableEntityExtender::class;
    $entity_storage = $this->getConfiguredEntityStorage($class);

    // Reset all counters.
    $GLOBALS[$class] = $state = [];

    $entity = $entity_storage->create(static::ENTITY_CREATE_DATA);
    $entity_storage->save($entity);

    try {
      $view_builder = $this->container
        ->get('entity_type.manager')
        ->getViewBuilder($entity->getEntityTypeId());

      static::assertTrue(\assert($view_builder instanceof EntityViewBuilderInterface));
      $build = $view_builder->view($entity);

      $this->container
        ->get('renderer')
        ->renderRoot($build);

      $state['alterRenderableData'] = 1;
      static::assertSame($GLOBALS[$class], $state);
    }
    catch (InvalidPluginDefinitionException $e) {
      static::assertSame(
        \sprintf('The "%s" entity type did not specify a view_builder handler.', $entity->getEntityTypeId()),
        $e->getMessage(),
      );
    }
  }

  /**
   * Tests access of the extender.
   *
   * @throws \Exception
   *
   * @see \Drupal\entity_extender\Plugin\EntityTypeExtender\EntityHandlerTraitAccess
   */
  public function testEntityHandlerTraitAccess(): void {
    $class = AccessEntityExtender::class;
    $entity_storage = $this->getConfiguredEntityStorage($class);

    // Reset all counters.
    $GLOBALS[$class] = $state = [];

    $entity = $entity_storage->create(static::ENTITY_CREATE_DATA);
    $account = $this->createMock(UserInterface::class);
    static::assertTrue(\assert($account instanceof UserInterface));

    foreach ([
      // Denied early.
      'forbidden' => FALSE,
      // Extender allows but core blocks.
      'allowed' => FALSE,
      // Extender is neutral but core blocks.
      'unknown' => FALSE,
      // Extender allows and core ignored.
      'allowed-immediately' => TRUE,
    ] as $operation => $expected) {
      static::assertSame($expected, $entity->access($operation, $account));
      $state[$operation] = 1;
      static::assertSame($GLOBALS[$class], $state);
    }
  }

  /**
   * Tests lifecycle of the extender.
   *
   * @throws \Exception
   *
   * @see \Drupal\entity_extender\Plugin\EntityTypeExtender\EntityHandlerTraitStorage
   */
  public function testEntityHandlerTraitStorage(): void {
    static::assertTrue($this->container->get('module_installer')->install(['language']));

    $langcode = 'xyz';
    $language_storage = $this->container
      ->get('entity_type.manager')
      ->getStorage('configurable_language');

    $language_storage->save($language_storage->create([
      'id' => $langcode,
      'label' => $langcode,
    ]));

    $class = LifecycleEntityExtender::class;
    $entity_storage = $this->getConfiguredEntityStorage($class);

    // Reset all counters.
    $GLOBALS[$class] = $state = [];

    $entity = $entity_storage->create(static::ENTITY_CREATE_DATA);
    $state['onCreate'] = 1;
    static::assertSame($GLOBALS[$class], $state);

    $entity_storage->save($entity);
    $state['onPreSave'] = 1;
    $state['onInsert'] = 1;
    static::assertSame($GLOBALS[$class], $state);

    $entity_storage->save($entity);
    $state['onPreSave']++;
    $state['onUpdate'] = 1;
    static::assertSame($GLOBALS[$class], $state);

    /* @see \Drupal\Core\Entity\ContentEntityBase::setNewRevision() */
    if ($entity->getEntityType()->getKey('revision')) {
      static::assertTrue(\assert($entity instanceof RevisionableInterface));
      static::assertTrue(\assert($entity_storage instanceof RevisionableStorageInterface));
      $revision = $entity_storage->createRevision($entity, FALSE);
      $state['onCreateRevision'] = 1;
      static::assertSame($GLOBALS[$class], $state);

      $entity_storage->save($revision);
      $state['onPreSave']++;
      $state['onUpdate']++;
      static::assertSame($GLOBALS[$class], $state);

      $entity_storage->deleteRevision($revision->getRevisionId());
      $state['onPreDeleteRevision'] = 1;
      $state['onPostDeleteRevision'] = 1;
      static::assertSame($GLOBALS[$class], $state);
    }

    $entity->addTranslation($langcode, $entity->toArray());
    $state['onCreateTranslation'] = 1;
    static::assertSame($GLOBALS[$class], $state);

    $entity_storage->save($entity);
    $state['onPreSave']++;
    $state['onUpdate']++;
    $state['onInsertTranslation'] = 1;
    static::assertSame($GLOBALS[$class], $state);

    // This action doesn't call a hook.
    $entity->removeTranslation($langcode);
    static::assertSame($GLOBALS[$class], $state);

    $entity_storage->save($entity);
    $state['onPreSave']++;
    $state['onUpdate']++;
    // The `onPostDeleteTranslation` is called only on save.
    $state['onPostDeleteTranslation'] = 1;
    static::assertSame($GLOBALS[$class], $state);

    $entity_storage->delete([$entity]);
    $state['onPreDelete'] = 1;
    $state['onPostDelete'] = 1;
    static::assertSame($GLOBALS[$class], $state);
  }

  /**
   * Tests that "entity_extender" overrides entity handlers properly.
   *
   * @param bool $custom
   *   The state of whether to test the default or custom implementation.
   *
   * @throws \Exception
   *
   * @dataProvider providerEntityHandlerOverrides
   */
  public function testEntityHandlerOverrides(bool $custom): void {
    $module_installer = $this->container->get('module_installer');

    if ($custom) {
      static::assertTrue(
        $module_installer->install([static::$provider]),
        'Required modules enabled successfully.',
      );
    }

    static::assertFalse(
      $this->container->get('entity_type.manager')->hasDefinition(static::$entityType),
      \sprintf('No "%s" definition until the module is installed.', static::$entityType),
    );
    static::assertTrue(
      $module_installer->install([static::$entityType]),
      \sprintf('The "%s" module has been installed.', static::$entityType),
    );

    // IMPORTANT: the "entity_type.manager" must not be saved to a variable
    // because it'll contain a stale instance after installing the modules.
    $entity_type = $this->container->get('entity_type.manager')->getDefinition(static::$entityType);
    static::assertTrue(\assert($entity_type instanceof EntityTypeInterface));
    $entity_type_extender_class = static::ENTITY_TYPE_EXTENDER;
    $entity_type_extender = new $entity_type_extender_class(['id' => static::$entityType]);
    static::assertTrue(\assert($entity_type_extender instanceof EntityTypeExtender));

    foreach ($entity_type_extender->getHandlers() as $handler) {
      static::assertSame(
        $custom ? $handler->replacingClass : $handler->defaultClass,
        $entity_type->getHandlerClass($handler->getType()),
      );
    }
  }

  /**
   * Returns the test suites.
   *
   * @return array
   *   The test suites.
   */
  public function providerEntityHandlerOverrides(): array {
    return [
      [FALSE],
      [TRUE],
    ];
  }

}
